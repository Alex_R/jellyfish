COMMON_OPTS=-rdynamic -Wall -Wextra \
src/*.c \
-I src \
-pthread


all:
	mkdir -p ./bin
	
	clang -g \
	$(COMMON_OPTS) \
	-o bin/jellyfish

release:
	mkdir -p ./bin
	
	clang -O3 -Ofast -march=native \
	$(COMMON_OPTS) \
	-o bin/jellyfish
