#ifndef PATH_H_INCLUDED
#define PATH_H_INCLUDED

#include <stdbool.h>


/* Function prototypes. */
int path_join(const char *path_a, const char *path_b, char *dest, size_t dest_size);

int path_pivot(const char *path, const char *source_prefix, const char *dest_prefix, char *dest, size_t dest_size);

int path_basename(const char *path, char *dest, size_t dest_size);

bool path_is_prefixed(const char *path, const char *prefix);
bool path_is_suffixed(const char *path, const char *suffix);

#endif
