#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <execinfo.h>
#include <signal.h>

#include "die.h"
#include "log.h"
#include "main.h"


static void _die_traceback(void) {
    /* _die_traceback
     * Generate a traceback using the backtrace() function and log it to stderr.
     */

    void *traceback_buf[DIE_MAX_TRACEBACK_SIZE];
    char **traceback_strings;
    int traceback_size, iter;

    /* Read the stack frames.
     * The standard specifies no error return value, so we just check that we've read at least 1 frame.
     */
    traceback_size = backtrace(traceback_buf, DIE_MAX_TRACEBACK_SIZE);
    if(traceback_size < 1) {
        erro("Failed to generate backtrace: backtrace() failed");
        return;
    }

    /* Convert the stack frames into human readable strings and write them to stderr. */
    traceback_strings = backtrace_symbols(traceback_buf, traceback_size);
    if(traceback_strings == NULL) {
        erro("Failed to generate backtrace: backtrace_symbols() failed");
        return;
    }

    erro("Stack traceback:");

    for(iter = 0; iter < traceback_size; iter++) {
        erro("%s", traceback_strings[iter]);
    }

    free(traceback_strings);
}


void _die_abort(void) {
    /* _die_abort
     * Reset the SIGABRT handler then abort.
     */

    struct sigaction act = {
        .sa_flags = SA_SIGINFO,
    };

    act.sa_sigaction = NULL;
    act.sa_handler = SIG_DFL;
    sigaction(SIGABRT, &act, NULL);

    abort();
}


void _die_internal(const char *file, int line_number, const char *format, ...) {
    /* _die_internal
     * Log an error message based on the supplied file, line_number, format, and variadic arguments to stderr then exit.
     * This function shouldn't be called directly.  Rather you should call the die() macro which adds the file and line_number arguments for you.
     *
     * @ const char *file        - The file in which the error occurred.
     * @ const char *line_number - The line in the file on which the error occurred.
     * @ const char *format      - The error message format.
     * @ ...                     - Extra variadic arguments used with the format to generate the message.
     */

    va_list vargs;

    /* It's a little ugly to allocate our message buffer statically on the stack like this, but we want to avoid
     * heap allocation as we don't know what the state of the process/system is before the call to die() (e.g. memory could be exhausted).
     */
    char message[DIE_MAX_MESSAGE_LENGTH];
    size_t partial_length;

    va_start(vargs, format);

    /* Write the boilerplate. */
    if(snprintf(message, DIE_MAX_MESSAGE_LENGTH, "%s: fatal error @%s:%d: ", MAIN_NAME, file, line_number) < 1) {
        erro("Failed to generate error message: snprintf() failed");
        _die_abort();
    }

    /* Generate message. */
    partial_length = strlen(message);
    if(vsnprintf(&message[partial_length], (DIE_MAX_MESSAGE_LENGTH - partial_length), format, vargs) < 0) {
        erro("Failed to generate error message: vsnprintf() failed");
        _die_abort();
    }

    erro("%s", message);

    /* Write the traceback to stderr */
    _die_traceback();

    va_end(vargs);

    _die_abort();
}
