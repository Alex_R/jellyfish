#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

/* Constants. */
#define MAIN_NAME      "jellyfish"
#define MAIN_VERSION   "v0.1.0"
#define MAIN_COPYRIGHT "Copyright (C) Alex Richman <alex@richman.io>"
#ifdef __OPTIMIZE__
#define MAIN_TYPE "release"
#else
#define MAIN_TYPE "debug"
#endif
#define MAIN_FULL_VERSION MAIN_NAME " " MAIN_VERSION "-" MAIN_TYPE


/* Type definitions. */
enum main_status_format {
    MAIN_STATUS_FMT_NONE,
    MAIN_STATUS_FMT_HLINE,
    MAIN_STATUS_FMT_MLINE,
    MAIN_STATUS_FMT_MSUM,
};

/* NB: Direction values need to be static for machine output. */
enum main_status_dir {
    MAIN_STATUS_DIR_LEFT  = 0,
    MAIN_STATUS_DIR_RIGHT = 1,
    MAIN_STATUS_DIR_INTER = 2,
};


/* Function prototypes. */
int main(int argc, char *argv[]);

#endif
