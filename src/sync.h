#ifndef SYNC_H_INCLUDED
#define SYNC_H_INCLUDED

#include <stdbool.h>

#include "ethread.h"
#include "equeue.h"
#include "index.h"


/* Type definitions. */
typedef int (sync_popfunc_t)(void *context, void **entry);
typedef int (sync_waitfunc_t)(void *context, struct timespec *timeout);

enum sync_action {
    SYNC_ACTION_NONE,
    SYNC_ACTION_COPY,
    SYNC_ACTION_DELETE,
};

struct sync {
    int left_dfd, right_dfd;

    void *left_context, *right_context;
    sync_popfunc_t *left_pop, *right_pop;
    sync_waitfunc_t *left_wait, *right_wait;

    enum sync_action left_action, right_action;

    struct timespec left_delay, right_delay;
    struct timespec left_root_time, right_root_time;

    struct equeue *left_out_queue, *right_out_queue;

    struct ethread_pool *left_thread_pool, *right_thread_pool;
    size_t left_workers_active, left_workers_failed;
    size_t right_workers_active, right_workers_failed;

    struct econd *next_cond;
};


/* Function prototypes. */
struct sync *sync_create(const char *left_root_path, const char *right_root_path,
                         size_t queue_size,
                         struct econd *next_cond,
                         void *left_context, void *right_context,
                         sync_popfunc_t *left_pop, sync_popfunc_t *right_pop,
                         sync_waitfunc_t *left_wait, sync_waitfunc_t *right_wait,
                         size_t left_worker_threads, size_t right_worker_threads,
                         enum sync_action left_action, enum sync_action right_action,
                         struct timespec left_delay, struct timespec right_delay);
void sync_destroy(struct sync *sync);

int sync_left_pop(struct sync *sync, struct index_entry **entry);
int sync_right_pop(struct sync *sync, struct index_entry **entry);

#endif
