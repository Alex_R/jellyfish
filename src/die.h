#ifndef DIE_H_INCLUDED
#define DIE_H_INCLUDED

/* Constants. */
#define DIE_MAX_MESSAGE_LENGTH 1024
#define DIE_MAX_TRACEBACK_SIZE 1024


/* Macros. */
#define die(format, ...) _die_internal(__FILE__, __LINE__, format, ##__VA_ARGS__)


/* Function prototypes. */
void _die_internal(const char *file, int line_number, const char *format, ...);

#endif
