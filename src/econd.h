#ifndef ECOND_INCLUDED
#define ECOND_INCLUDED

#include <pthread.h>
#include <time.h>

#include "eatomic.h"


/* Type definitions. */
struct econd {
    pthread_cond_t cond;
    pthread_mutex_t mutex;

    size_t threads;
    size_t signaled;
};


/* Function prototypes */
struct econd *econd_create(size_t threads);
void econd_destroy(struct econd *cond);

void econd_wait(struct econd *cond, struct timespec *timeout);
void econd_signal(struct econd *cond);
void econd_broadcast(struct econd *cond);

#endif
