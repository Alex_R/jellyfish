#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

#include "elock.h"
#include "die.h"


struct elock *elock_create(int preference) {
    /* elock_create
     * Return a newly allocated and initialised elock.
     *
     * @ int preference - The "preference" of the lock towards waiting read/write threads.  See pthread_rwlockattr_setkind_np(3).
     *
     * # struct elock * - The new elock.
     */

    struct elock *lock;
    int res;

    /* Allocate the elock structure memory. */
    lock = malloc(sizeof(struct elock));
    if(lock == NULL) {
        die("elock_create() failed: malloc(%ld) failed: %s", sizeof(struct elock), strerror(errno));
    }

    res = pthread_rwlockattr_init(&lock->attr);
    if(res != 0) {
        die("elock_create() failed: pthread_rwlockattr_init() failed: %s", strerror(res));
    }

    if(       preference == ELOCK_PREFER_READ) {
        res = pthread_rwlockattr_setkind_np(&lock->attr, PTHREAD_RWLOCK_PREFER_READER_NP);
    } else if(preference == ELOCK_PREFER_WRITE) {
        res = pthread_rwlockattr_setkind_np(&lock->attr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);
    } else {
        die("elock_create() failed: Invalid preference supplied");
    }
    if(res != 0) {
        die("elock_create() failed: pthread_rwlockattr_setkind_np() failed: %s", strerror(res));
    }


    res = pthread_rwlock_init(&lock->lock, &lock->attr);
    if(res != 0) {
        die("elock_create() failed: pthread_rwlock_init(%p) failed: %s", &lock->lock, strerror(res));
    }

    return lock;
}

void elock_destroy(struct elock *lock) {
    /* elock_destroy
     * Destroy the supplied lock.
     *
     * @ struct elock *lock - The lock to destroy.
     */

    int res;

    res = pthread_rwlock_destroy(&lock->lock);
    if(res != 0) {
        die("elock_destroy(%p) failed: pthread_rwlock_destroy(%p) failed: %s", lock, &lock->lock, strerror(res));
    }

    free(lock);
}


void elock_acquire_write(struct elock *lock) {
    /* elock_aqurie_write
     * Aquire the write lock on the supplied lock.
     *
     * @ struct elock *lock - The lock to acquire the write lock on.
     */

    int res;

    res = pthread_rwlock_wrlock(&lock->lock);
    if(res != 0) {
        die("elock_acquire_write(%p) failed: pthread_rwlock_wrlock(%p) failed: %s", lock, &lock->lock, strerror(res));
    }
}

int elock_acquire_write_try(struct elock *lock) {
    /* elock_acquire_write_try
     * Attempt to acquire the write lock on the supplied lock.
     *
     * @ struct elock *lock - The lock to acquire the write lock on.
     *
     * # int - 0 on success, -1 if the lock is not available.
     */

    int res;

    res = pthread_rwlock_trywrlock(&lock->lock);
    if(res != 0) {
        if(res == EBUSY) {
            goto busy;
        }

        die("elock_acquire_write_try(%p) failed: pthread_rwlock_trywrlock(%p) failed: %s", lock, &lock->lock, strerror(res));
    }

    return 0;

    busy:
        return -1;
}

void elock_acquire_read(struct elock *lock) {
    /* elock_acquire_read
     * Aquire the read lock on the supplied lock.
     *
     * @ struct elock *lock - The lock to acquire the read lock on.
     */

    int res;

    res = pthread_rwlock_rdlock(&lock->lock);
    if(res != 0) {
        die("elock_acquire_read(%p) failed: pthread_rwlock_rdlock(%p) failed: %s", lock, &lock->lock, strerror(res));
    }
}

int elock_acquire_read_try(struct elock *lock) {
    /* elock_acquire_read_try
     * Attempt to acquire the read lock on the supplied lock.
     *
     * @ struct elock *lock - The lock to acquire the read lock on.
     *
     * # int - 0 on success, -1 if the lock is not available.
     */

    int res;

    res = pthread_rwlock_tryrdlock(&lock->lock);
    if(res != 0) {
        if(res == EBUSY) {
            goto busy;
        }

        die("elock_acquire_read_try(%p) failed: pthread_rwlock_tryrdlock(%p) failed: %s", lock, &lock->lock, strerror(res));
    }

    return 0;

    busy:
        return -1;
}

void elock_release(struct elock *lock) {
    /* elock_release
     * Release the read or write lock on the supplied lock.
     *
     * @ struct elock *lock - The lock to release the read or write lock on.
     */

    int res;

    res = pthread_rwlock_unlock(&lock->lock);
    if(res != 0) {
        die("elock_release(%p) failed: pthread_rwlock_release(%p) failed: %s", lock, &lock->lock, strerror(res));
    }
}
