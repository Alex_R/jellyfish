#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <stdint.h>

#include "die.h"
#include "econd.h"
#include "eatomic.h"


struct econd *econd_create(size_t threads) {
    /* econd_create
     * Allocate and initialise a new econd.
     *
     * @ size_t threads - The number of threads that will be using the cond.
     *
     * # struct econd * - The new econd.
     */

    struct econd *cond;
    pthread_condattr_t attr;
    int res;

    cond = malloc(sizeof(struct econd));
    if(cond == NULL) {
        die("econd_create() failed: malloc(%ld) failed: %s", sizeof(struct econd), strerror(errno));
    }

    res = pthread_condattr_init(&attr);
    if(res != 0) {
        die("econd_create() failed: pthread_condattr_init() failed: %s", strerror(res));
    }

    res = pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);
    if(res != 0) {
        die("econd_create() failed: pthread_condattr_setclock() failed: %s", strerror(res));
    }

    res = pthread_cond_init(&cond->cond, &attr);
    if(res != 0) {
        die("econd_create() failed: pthread_cond_init(%p) failed: %s", &cond->cond, strerror(res));
    }

    res = pthread_condattr_destroy(&attr);
    if(res != 0) {
        die("econd_create() failed: pthraed_condattr_destroy() failed: %s", strerror(res));
    }

    res = pthread_mutex_init(&cond->mutex, NULL);
    if(res != 0) {
        die("econd_create() failed: pthread_mutex_init(%p) failed: %s", &cond->mutex, strerror(res));
    }

    cond->threads = threads;
    cond->signaled = 0;

    return cond;
}

void econd_destroy(struct econd *cond) {
    /* econd_destroy
     * Destroy the supplied cond.
     *
     * @ struct econd *cond - The cond to destroy.
     */

    int res;

    res = pthread_cond_destroy(&cond->cond);
    if(res != 0) {
        die("econd_destroy(%p) failed: pthread_cond_destroy(%p) failed: %s", cond, &cond->cond, strerror(res));
    }

    res = pthread_mutex_destroy(&cond->mutex);
    if(res != 0) {
        die("econd_destroy(%p) failed: pthread_mutex_destroy(%p) failed: %s", cond, &cond->mutex, strerror(res));
    }

    free(cond);
}


bool _econd_wait_signaled_decrement(size_t *signaled) {
    /* _econd_wait_signaled_sub
     * Decrement the supplied signaled attribute, preventing decrement below 0.
     *
     * @ size_t *signaled - The signaled attribute to decrement.
     *
     * # bool - True if the decrement was successful, False otherwise.
     */

    size_t value;

    value = eatomic_load(signaled);
    while(1) {
        if(value > 0) {
            if(eatomic_compare_exchange(signaled, &value, (value - 1)) == true) {
                return true;
            }
        } else {
            return false;
        }
    }
}

void econd_wait(struct econd *cond, struct timespec *timeout) {
    /* econd_wait
     * Wait for the supplied condition to be signaled, or for timeout, whichever is sooner.
     *
     * @ struct econd *cond       - The cond to wait for.
     * @ struct timespec *timeout - The timeout to wait for.
     */

    int res;
    struct timespec current_time, abstimeout;

    if(clock_gettime(CLOCK_MONOTONIC, &current_time) == -1) {
        die("econd_wait(%p, %p) failed: clock_gettime() failed: %s", cond, timeout, strerror(errno));
    }

    abstimeout.tv_nsec = (current_time.tv_nsec + timeout->tv_nsec);
    abstimeout.tv_sec  = (current_time.tv_sec  + timeout->tv_sec + (abstimeout.tv_nsec / (uint64_t)1e9));
    abstimeout.tv_nsec = (abstimeout.tv_nsec % (uint64_t)1e9);

    /* Note that we lock the mutex before checking the signaled attribute so that the wait is protected from a race between it and a signal/broadcast.
     * If we didn't do this then a raced call could find a zero signaled attribute but enter the wait just after the condition is signaled, meaning we
     * still get a lost wakeup.
     */
    res = pthread_mutex_lock(&cond->mutex);
    if(res != 0) {
        die("econd_wait(%p, %p) failed: pthread_mutex_lock(%p) failed: %s", cond, timeout, &cond->mutex, strerror(res));
    }

    /* Return early if the cond was signaled since we last hit it (avoiding lost wakeup). */
    if(_econd_wait_signaled_decrement(&cond->signaled)) {
        goto unlock_mutex_and_return;
    }

    res = pthread_cond_timedwait(&cond->cond, &cond->mutex, &abstimeout);
    if(res != 0) {
        if(res != ETIMEDOUT) {
            die("econd_wait(%p, %p) failed: pthread_cond_timedwait(%p, %p, %p) failed: %s", cond, timeout, &cond->cond, &cond->mutex, &abstimeout, strerror(res));
        }
    }

    _econd_wait_signaled_decrement(&cond->signaled);

    unlock_mutex_and_return:
        res = pthread_mutex_unlock(&cond->mutex);
        if(res != 0) {
            die("econd_wait(%p, %p) failed: pthread_mutex_unlock(%p) failed: %s", cond, timeout, &cond->mutex, strerror(res));
        }
}

void econd_signal(struct econd *cond) {
    /* econd_signal
     * Signal the supplied condition.  This will wake up 1 waiting thread.
     *
     * @ struct econd *cond - The cond to signal.
     */

    int res;

    res = pthread_mutex_lock(&cond->mutex);
    if(res != 0) {
        die("econd_signal(%p) failed: pthread_mutex_lock(%p) failed: %s", cond, &cond->mutex, strerror(res));
    }

    res = pthread_cond_signal(&cond->cond);
    if(res != 0) {
        die("econd_signal(%p) failed: pthread_cond_signal(%p) failed: %p", cond, &cond->cond, strerror(res));
    }

    eatomic_add(&cond->signaled, 1);

    res = pthread_mutex_unlock(&cond->mutex);
    if(res != 0) {
        die("econd_signal(%p) failed: pthread_mutex_unlock(%p) failed: %s", cond, &cond->mutex, strerror(res));
    }
}

void econd_broadcast(struct econd *cond) {
    /* econd_broadcast
     * Broadcast the supplied condition.  This will wake up all waiting threads.
     *
     * @ struct econd *cond - The cond to broadcast.
     */

    int res;

    res = pthread_mutex_lock(&cond->mutex);
    if(res != 0) {
        die("econd_broadcast(%p) failed: pthread_mutex_lock(%p) failed: %s", cond, &cond->mutex, strerror(res));
    }

    res = pthread_cond_broadcast(&cond->cond);
    if(res != 0) {
        die("econd_broadcast(%p) failed: pthread_cond_broadcast(%p) failed: %p", cond, &cond->cond, strerror(res));
    }

    eatomic_add(&cond->signaled, cond->threads);

    res = pthread_mutex_unlock(&cond->mutex);
    if(res != 0) {
        die("econd_broadcast(%p) failed: pthread_mutex_unlock(%p) failed: %s", cond, &cond->mutex, strerror(res));
    }
}
