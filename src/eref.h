#ifndef EREF_H_INCLUDED
#define EREF_H_INCLUDED

/* Type definitions */
typedef void (eref_destructor_function_t)(void *object);

struct eref {
    size_t references;
    void *object;
    eref_destructor_function_t *destructor;
};


/* Function prototypes. */
void eref_init(struct eref *ref, void *object, eref_destructor_function_t *destructor);

int eref_acquire(struct eref *ref);
void eref_release(struct eref *ref);

#endif
