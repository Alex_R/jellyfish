#ifndef ELOCK_INCLUDED
#define ELOCK_INCLUDED

#include <pthread.h>


/* Constants. */
#define ELOCK_PREFER_READ  0
#define ELOCK_PREFER_WRITE 1



/* Type definitions. */
struct elock {
    pthread_rwlockattr_t attr;
    pthread_rwlock_t lock;
};


/* Function prototypes */
struct elock *elock_create(int preference);
void elock_destroy(struct elock *lock);

void elock_acquire_write(struct elock *lock);
int elock_acquire_write_try(struct elock *lock);
void elock_acquire_read(struct elock *lock);
int elock_acquire_read_try(struct elock *lock);
void elock_release(struct elock *lock);

#endif
