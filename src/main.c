#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "main.h"
#include "args.h"
#include "log.h"
#include "index.h"
#include "sync.h"
#include "etime.h"


struct _main_context {
    struct index *left_index, *right_index;

    struct index_delta *index_delta;

    struct sync *sync;

    struct econd *index_delta_cond, *main_cond;

    struct {
        size_t left_success, left_failed, left_bytes;
        size_t right_success, right_failed, right_bytes;
        size_t intersect;
    } status_sum;
    struct timespec status_sum_last;
};


static int _main_init(struct _main_context *context, struct args *arguments) {
    size_t index_queue_part = (arguments->index_pipeline_size / 5); /* Left, right, and 3 delta queues. */
    enum index_delta_inter_auto_pref inter_auto_pref;

    context->index_delta_cond = econd_create(1);
    context->main_cond = econd_create(1);

    context->left_index = index_create(index_queue_part, arguments->index_left_threads, context->index_delta_cond);
    if(context->left_index == NULL) {
        erro("_main_init() failed: index_create(left) failed");
        goto free_cond_and_error;
    }

    context->right_index = index_create(index_queue_part, arguments->index_right_threads, context->index_delta_cond);
    if(context->right_index == NULL) {
        erro("_main_init() failed: index_create(right) failed");
        goto free_leftindex_cond_and_error;
    }

    if(index_root_add(context->left_index, arguments->left_root_path, arguments->index_max_depth) == -1) {
        erro("_main_init() failed: index_root_add(left, '%s') failed", arguments->left_root_path);
        goto free_rightindex_leftindex_cond_and_error;
    }

    if(index_root_add(context->right_index, arguments->right_root_path, arguments->index_max_depth) == -1) {
        erro("_main_init() failed: index_root_add(right, '%s') failed", arguments->right_root_path);
        goto free_rightindex_leftindex_cond_and_error;
    }

    /* Inter side preference, for AUTO resolution.  See help text for rationale. */
    if(arguments->sync_left_action == SYNC_ACTION_COPY && arguments->sync_right_action == SYNC_ACTION_COPY) {
        inter_auto_pref = INDEX_DELTA_INTER_AUTO_TIME;
    } else if(arguments->sync_left_action == SYNC_ACTION_COPY && arguments->sync_right_action != SYNC_ACTION_COPY) {
        inter_auto_pref = INDEX_DELTA_INTER_AUTO_LEFT;
    } else if(arguments->sync_left_action != SYNC_ACTION_COPY && arguments->sync_right_action == SYNC_ACTION_COPY) {
        inter_auto_pref = INDEX_DELTA_INTER_AUTO_RIGHT;
    } else {
        inter_auto_pref = INDEX_DELTA_INTER_AUTO_NONE;
    }

    context->index_delta = index_delta_create(context->left_index, context->right_index, index_queue_part,
                                              context->index_delta_cond, context->main_cond,
                                              arguments->index_check_size,
                                              arguments->index_check_mtime, arguments->index_mtime_window,
                                              arguments->index_inter_resolve, inter_auto_pref);
    if(context->index_delta == NULL) {
        erro("_main_init() failed: index_delta_create() failed");
        goto free_rightindex_leftindex_cond_and_error;
    }

    context->sync = sync_create(arguments->left_root_path, arguments->right_root_path,
                                (arguments->sync_pipeline_size / 2), /* Pipeline size shared between left/right. */
                                context->main_cond,
                                context->index_delta, context->index_delta,
                                (sync_popfunc_t *)index_delta_comp_alpha_pop,
                                (sync_popfunc_t *)index_delta_comp_beta_pop,
                                (sync_waitfunc_t *)index_delta_comp_alpha_wait,
                                (sync_waitfunc_t *)index_delta_comp_beta_wait,
                                arguments->sync_left_threads, arguments->sync_right_threads,
                                arguments->sync_left_action, arguments->sync_right_action,
                                arguments->sync_left_delay, arguments->sync_right_delay);
    if(context->sync == NULL) {
        erro("_main_init() failed: sync_create() failed");
        goto free_delta_rightindex_leftindex_cond_and_error;
    }

    return 0;

    free_delta_rightindex_leftindex_cond_and_error:
        index_delta_destroy(context->index_delta);
    free_rightindex_leftindex_cond_and_error:
        index_destroy(context->right_index);
    free_leftindex_cond_and_error:
        index_destroy(context->left_index);
    free_cond_and_error:
        econd_destroy(context->main_cond);
        econd_destroy(context->index_delta_cond);
        return -1;
}

static void _main_destroy(struct _main_context *context) {
    sync_destroy(context->sync);

    index_delta_destroy(context->index_delta);

    index_destroy(context->right_index);
    index_destroy(context->left_index);

    econd_destroy(context->main_cond);
    econd_destroy(context->index_delta_cond);
}

static int _main_status_hline(struct index_entry *entry, enum main_status_dir direction) {
    switch(direction) {
        case MAIN_STATUS_DIR_LEFT:
            return fprintf(stdout, "%1$c%1$c> %2$s\n", (entry->sync_status == 0 ? '-' : '!'), entry->path);

        case MAIN_STATUS_DIR_RIGHT:
            return fprintf(stdout, "<%1$c%1$c %2$s\n", (entry->sync_status == 0 ? '-' : '!'), entry->path);

        case MAIN_STATUS_DIR_INTER:
            return fprintf(stdout, "--- %s\n", entry->path);
    };
}

static int _main_status_mline(struct index_entry *entry, enum main_status_dir direction) {
    size_t buf_max = (PATH_MAX + NAME_MAX), buf_size, bytes_written;
    ssize_t write_size;
    char buf[buf_max];

    buf_size = 0;

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%d", direction)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%d", entry->sync_status)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%s", entry->path)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    buf[buf_size] = '\0';
    buf_size += 1;

    bytes_written = 0;
    while(bytes_written < buf_size) {
        write_size = write(STDOUT_FILENO, &buf[bytes_written], (buf_size - bytes_written));
        if(write_size == -1) {
            goto error;
        }

        bytes_written += write_size;
    }

    return 0;

    error:
        return -1;
}

static int _main_status_msum(struct _main_context *context,
                             struct index_entry *entry, enum main_status_dir direction) {
    struct timespec curr, delta;
    size_t buf_max = PATH_MAX, buf_size, bytes_written;
    ssize_t write_size;
    char buf[buf_max];

    switch(direction) {
        case MAIN_STATUS_DIR_LEFT:
            if(entry->sync_status == 0) {
                context->status_sum.left_success += 1;
                context->status_sum.left_bytes += entry->st.st_size;
            } else {
                context->status_sum.left_failed += 1;
            }
            break;

        case MAIN_STATUS_DIR_RIGHT:
            if(entry->sync_status == 0) {
                context->status_sum.right_success += 1;
                context->status_sum.right_bytes += entry->st.st_size;
            } else {
                context->status_sum.right_failed += 1;
            }
            break;

        case MAIN_STATUS_DIR_INTER:
            context->status_sum.intersect += 1;
            break;
    }

    if(_etime_get(&curr) == -1) {
        goto error;
    }
    if(context->status_sum_last.tv_sec != 0) {
        _etime_delta(&curr, &context->status_sum_last, &delta);
        if(_etime_delta(&delta, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e8}, NULL) == 1) {
            return 0;
        }
    }
    memcpy(&context->status_sum_last, &curr, sizeof(struct timespec));

    buf_size = 0;

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.left_success)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.left_failed)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.left_bytes)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.right_success)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.right_failed)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.right_bytes)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    if((write_size = snprintf(&buf[buf_size], (buf_max - buf_size), "%zu", context->status_sum.intersect)) < 1) {
        goto error;
    }
    buf_size += (write_size + 1);

    buf[buf_size] = '\0';
    buf_size += 1;

    bytes_written = 0;
    while(bytes_written < buf_size) {
        write_size = write(STDOUT_FILENO, &buf[bytes_written], (buf_size - bytes_written));
        if(write_size == -1) {
            goto error;
        }

        bytes_written += write_size;
    }

    return 0;

    error:
        return -1;
}

static int _main_status(struct args *arguments, struct _main_context *context,
                        struct index_entry *entry, enum main_status_dir direction) {
    switch(arguments->sync_status_output) {
        case MAIN_STATUS_FMT_NONE:
            return 0;

        case MAIN_STATUS_FMT_HLINE:
            return _main_status_hline(entry, direction);

        case MAIN_STATUS_FMT_MLINE:
            return _main_status_mline(entry, direction);

        case MAIN_STATUS_FMT_MSUM:
            return _main_status_msum(context, entry, direction);
    };
}

int main(int argc, char *argv[]) {
    struct args *arguments;
    struct _main_context context;
    struct index_entry *entry;
    int ret, waiting, finished;

    arguments = args_parse(argc, argv);
    if(arguments == NULL) {
        goto error;
    }

    if(arguments->left_root_path[0] == '\0') {
        args_destroy(arguments);
        return EXIT_SUCCESS;
    }

    log_level_set(arguments->log_level);


    /* Create left/right roots if they don't already exist. */
    if(mkdir(arguments->left_root_path, 0755) == -1) {
        if(errno != EEXIST) {
            erro("_main() failed: mkdir('%s') failed: %s", arguments->left_root_path, strerror(errno));
            goto free_args_and_error;
        }
    }
    if(mkdir(arguments->right_root_path, 0755) == -1) {
        if(errno != EEXIST) {
            erro("_main() failed: mkdir('%s') failed: %s", arguments->right_root_path, strerror(errno));
            goto free_args_and_error;
        }
    }


    memset(&context, 0, sizeof(struct _main_context));
    if(_main_init(&context, arguments) == -1) {
        goto free_args_and_error;
    }


    while(1) {
        waiting = 0; finished = 0;

        ret = sync_left_pop(context.sync, &entry);
        if(ret == -1) {
            erro("main() failed: sync_left_pop() failed");
            goto free_context_args_and_error;
        }
        waiting  += (ret == 1 || ret == 2) ? 1 : 0;
        finished += (ret == 2) ? 1 : 0;
        if(ret == 0) {
            _main_status(arguments, &context, entry, MAIN_STATUS_DIR_LEFT);
            index_entry_destroy(entry);
        }


        ret = sync_right_pop(context.sync, &entry);
        if(ret == -1) {
            erro("main() failed: sync_right_pop() failed");
            goto free_context_args_and_error;
        }
        waiting  += (ret == 1 || ret == 2) ? 1 : 0;
        finished += (ret == 2) ? 1 : 0;
        if(ret == 0) {
            _main_status(arguments, &context, entry, MAIN_STATUS_DIR_RIGHT);
            index_entry_destroy(entry);
        }


        ret = index_delta_intersect_pop(context.index_delta, &entry);
        if(ret == -1) {
            erro("main() failed: index_delta_intersect_pop() failed");
            goto free_context_args_and_error;
        }
        waiting  += (ret == 1 || ret == 2) ? 1 : 0;
        finished += (ret == 2) ? 1 : 0;
        if(ret == 0) {
            _main_status(arguments, &context, entry, MAIN_STATUS_DIR_INTER);
            index_entry_destroy(entry);
        }


        if(finished == 3) {
            debu("sync completed");
            break;
        }
        if(waiting == 3) {
            econd_wait(context.main_cond, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e6});
            continue;
        }
    }

    _main_destroy(&context);
    args_destroy(arguments);
    return EXIT_SUCCESS;

    free_context_args_and_error:
        _main_destroy(&context);
    free_args_and_error:
        args_destroy(arguments);
    error:
        return EXIT_FAILURE;
}
