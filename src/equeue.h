#ifndef EQUEUE_H_INCLUDED
#define EQUEUE_H_INCLUDED

#include "eatomic.h"
#include "econd.h"
#include "ethread.h"


/* Type definitions. */
struct equeue_node {
    struct equeue_node *next, *next_block;
    void *data;
    eatomic_lock_t lock;
};

struct equeue {
    void *node_region;

    struct equeue_node *head, *tail;
    size_t max_size, block_size, allo_size, true_size;
    size_t threads;

    struct econd *cond;
};


/* Function prototypes. */
struct equeue *equeue_create(size_t max_size, size_t block_size, size_t threads);
void equeue_destroy(struct equeue *queue);

int equeue_push(struct equeue *queue, void *element);
int equeue_pop(struct equeue *queue, void **element);

int equeue_push_block(struct equeue *queue, void **block);
int equeue_pop_block(struct equeue *queue, void **block);

size_t equeue_push_many(struct equeue *queue, void **elements, size_t elements_size);
size_t equeue_pop_many(struct equeue *queue, void **elements, size_t elements_size);

size_t equeue_get_size(struct equeue *queue);

void equeue_wait(struct equeue *queue, struct timespec *timeout);
void equeue_signal(struct equeue *queue);

#endif
