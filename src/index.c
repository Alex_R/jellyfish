#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <syscall.h>

#include "index.h"
#include "log.h"
#include "die.h"
#include "equeue.h"
#include "econd.h"
#include "ethread.h"
#include "path.h"
#include "eref.h"
#include "elock.h"
#include "etime.h"


/* "You will need to define the linux_dirent or linux_dirent64 structure yourself." - getdents64(2). */
struct _linux_dirent64 {
    uint64_t d_ino;
    uint64_t d_off;
    unsigned short d_reclen;
    unsigned char d_type;
    char d_name[];
};

struct _linux_direntx {
    char name[NAME_MAX];
};


static void _index_worker(struct ethread *thread);

struct index *index_create(size_t queue_size, size_t worker_threads, struct econd *next_cond) {
    /* index_create
     * Create a new index, starting from the supplied root path and recursing until the supplied max depth.
     *
     * @ size_t queue_size       - The size of the internal index queue.
     * @ size_t worker_threads   - The number of worker threads to spawn.
     * @ struct econd *next_cond - The condition to signal when items are pushed into the index's queues.
     *
     * # struct index * - The index context, or NULL on error.
     */

    struct index *index;

    index = malloc(sizeof(struct index));
    if(index == NULL) {
        erro("index_create(%zu, %zu) failed: malloc(%zu) failed: %s", queue_size, worker_threads,
                                                                      sizeof(struct index), strerror(errno));
        goto error;
    }

    index->root_sequential_alloc = 0;
    index->root_sequential_done  = 0;

    index->workers_active = worker_threads;
    index->workers_failed = 0;

    index->root_queue = equeue_create(queue_size, 1, worker_threads);
    index->entry_queue = equeue_create(queue_size, 1, worker_threads);

    index->root_sequential_cond = econd_create(worker_threads);
    index->root_queue_lock = elock_create(ELOCK_PREFER_READ);

    index->next_cond = next_cond;

    index->thread_pool = ethread_pool_create(_index_worker, worker_threads, "index", (void *)index);

    return index;

    error:
        return NULL;
}

void index_destroy(struct index *index) {
    /* index_destroy
     * Destroy the supplied index.
     *
     * @ struct index *index - The index to destroy.
     */

    struct index_entry *entry;
    struct index_root *root;

    ethread_pool_destroy_signal(index->thread_pool, index->root_queue->cond);

    econd_destroy(index->root_sequential_cond);
    elock_destroy(index->root_queue_lock);

    while(equeue_get_size(index->entry_queue) > 0) {
        if(equeue_pop(index->entry_queue, (void **)&entry) == 0) {
            index_entry_destroy(entry);
        }
    }
    equeue_destroy(index->entry_queue);

    while(equeue_get_size(index->root_queue) > 0) {
        if(equeue_pop(index->root_queue, (void **)&root) == 0) {
            eref_release(&root->reference);
        }
    }
    equeue_destroy(index->root_queue);

    free(index);
}


int index_entry_pop(struct index *index, struct index_entry **entry) {
    /* index_entry_pop
     * Pop an entry from the entry queue of the supplied index.
     * The caller must later destroy the entry using index_entry_destroy().
     * Note that a return value of 1 indicating that the queue is empty does not mean that the indexing is finished.
     * Note that if another caller thread adds a root to an otherwise empty index (via index_root_add()) while
     * this call is executing the two may race and erroneously return indicating that the indexing is finished.
     *
     * @ struct index *index       - The index to pop the entry off of.
     * @ struct index_entry *entry - A pointer to an index_entry ptr to store the entry ptr in.
     *
     * # int - 0 on success, 1 if the queue is (currently) empty, 2 if the indexing is finished,
               -1 if one or more worker threads have failed.
     */

    if(eatomic_load(&index->workers_failed) != 0) {
        goto error;
    }

    if(equeue_pop(index->entry_queue, (void **)entry) == -1) {
        if(eatomic_load(&index->workers_active) == 0 &&
           (eatomic_load(&index->root_sequential_alloc) == eatomic_load(&index->root_sequential_done))) {
            return 2;
        }

        return 1;
    }

    return 0;

    error:
        return -1;
}

void index_entry_wait(struct index *index, struct timespec *timeout) {
    equeue_wait(index->entry_queue, timeout);
}

static struct index_entry *_index_entry_alloc(void) {
    /* _index_entry_alloc
     * Allocate (but do not initialise) a new index entry.
     * The caller must initialise the entry.
     *
     * # struct index_entry * - The newly allocated index entry, or NULL on error.
     */

    struct index_entry *entry;

    entry = malloc(sizeof(struct index_entry));
    if(entry == NULL) {
        goto error;
    }

    return entry;

    error:
        return NULL;
}

void index_entry_destroy(struct index_entry *entry) {
    /* index_entry_destroy
     * Destroy the supplied index entry.
     *
     * @ struct index_entry *entry - The 
     */

    eref_release(&entry->root->reference);

    free(entry);
}


static void _index_root_destroy(struct index_root *root) {
    /* _index_root_destroy
     * Destroy the supplied root.
     *
     * @ struct index_root *root - The root to destroy.
     */

    if(root->dfd != -1) {
        if(close(root->dfd) == -1) {
            erro("_index_root_destroy(%p) failed: close(%d) failed: %s", root, root->dfd, strerror(errno));
        }
    }

    if(root->parent != NULL) {
        eref_release(&root->parent->reference);
    }

    free(root);
}

static int _index_root_add(struct index *index, struct index_root *parent,
                           const char *root_path, size_t max_depth) {
    /* _index_root_add
     * Add a new root entry to the supplied index's root queue (roots to be indexed).
     * A reference is taken to the parent before returning (if it's not NULL).
     *
     * @ struct index *index       - The index object
     * @ struct index_root *parent - The parent root, or NULL if this is a root-root.
     * @ const char *root_path     - The root path to start indexing from (relative to parent).
     * @ size_t max_depth          - The maximum depth to recurse to.  0 to disable recursion, -1 for no limit.
     *
     * # int - 0 on success, -1 on error, -2 if the queue is full.
     */

    struct index_root *root;

    root = malloc(sizeof(struct index_root));
    if(root == NULL) {
        erro("_index_root_add(%p, %p, '%s', %zu) failed: malloc(%zu) failed: %s", index, parent,
                                                                                  root_path, max_depth,
                                                                                  sizeof(struct index_root),
                                                                                  strerror(errno));
        goto error;
    }

    root->parent = parent;
    if(root->parent != NULL) {
        if(eref_acquire(&root->parent->reference) == -1) {
            erro("_index_root_add(%p, %p, '%s', %zu) failed: eref_acquire() failed", index, parent,
                                                                                     root_path, max_depth);
            goto free_root_and_error;
        }
    }
    root->dfd = -1;

    strncpy(root->path, root_path, PATH_MAX);

    if(root->parent == NULL) {
        strncpy(root->full_path, root->path, PATH_MAX);
    } else {
        if(root->parent->parent == NULL) {
            strncpy(root->full_path, root->path, PATH_MAX);
        } else {
            if(path_join(root->parent->full_path, root->path, root->full_path, PATH_MAX) == -1) {
                erro("_index_root_add(%p, %p, '%s', %zu) failed: path_join('%s', '%s') failed", index, parent,
                                                                                              root_path, max_depth,
                                                                                              root->parent->full_path,
                                                                                              root->path);
                goto release_parent_free_root_and_error;
            }
        }
    }

    root->max_depth = max_depth;

    eref_init(&root->reference, root, (eref_destructor_function_t *)&_index_root_destroy);

    elock_acquire_write(index->root_queue_lock);
    root->sequential_id = eatomic_add(&index->root_sequential_alloc, 1);

    debu("[index] add root '%s'", root->full_path);
    if(equeue_push(index->root_queue, root) == -1) {
        debu("_index_root_add(%p, %p, '%s', %zu) failed: equeue_push() failed", index, parent,
                                                                                root_path, max_depth);
        eatomic_sub(&index->root_sequential_alloc, 1);
        elock_release(index->root_queue_lock);
        goto release_parent_free_root_and_full;
    }
    elock_release(index->root_queue_lock);
    equeue_signal(index->root_queue);

    return 0;

    release_parent_free_root_and_full:
        if(root->parent != NULL) {
            eref_release(&root->parent->reference);
        }
        free(root);
        return -2;

    release_parent_free_root_and_error:
        if(root->parent != NULL) {
            eref_release(&root->parent->reference);
        }
    free_root_and_error:
        free(root);
    error:
        return -1;
}

int index_root_add(struct index *index, const char *root_path, size_t max_depth) {
    /* index_root_add
     * Add a new root entry to the supplied index's root queue (roots to be indexed).
     *
     * @ struct index *index   - The index object
     * @ const char *root_path - The root path to start indexing from.
     * @ size_t max_depth      - The maximum depth to recurse to.  0 to disable recursion, -1 for no limit.
     *
     * # int - 0 on success, -1 on error, -2 if the queue is full.
     */

    return _index_root_add(index, NULL, root_path, max_depth);
}

static int _index_root_sequential_wait(struct ethread *thread, struct index *index, size_t sequential_id) {
    while(sequential_id != eatomic_load(&index->root_sequential_done)) {
        if(!ethread_running(thread)) {
            erro("_index_root_sequential_wait() failed: Thread interrupted");
            goto error;
        }

        debu("[index] waiting for sequential root sync");
        econd_wait(index->root_sequential_cond, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e6});
    }

    return 0;

    error:
        return -1;
}


static int _index_entry_blocking_push(struct ethread *thread, struct equeue *queue, struct index_entry *entry) {
    /* _index_entry_blocking_push
     * Push the supplied entry into the supplied queue, blocking if the queue is full.
     *
     * @ struct ethread *thread    - The thread to check the running state of.
     * @ struct equeue *queue      - The queue to push the entry into.
     * @ struct index_entry *entry - The entry to push.
     *
     * # int - 0 on success, -1 on error.
     */

    while(1) {
        if(!ethread_running(thread)) {
            erro("_index_entry_blocking_push() failed: Thread interrupted");
            goto error;
        }

        if(equeue_push(queue, entry) == -1) {
            usleep(10);
            continue;
        }

        equeue_signal(queue);
        break;
    }

    return 0;

    error:
        return -1;
}


static int _index_worker_enumerate_sort(const void *a, const void *b) {
    return strcmp(((struct _linux_direntx *)a)->name, ((struct _linux_direntx *)b)->name);
}

static int _index_worker_enumerate(struct ethread *thread, struct index *index, struct index_root *root,
                                   struct _linux_direntx **entbuffer, size_t *entbuffer_size,
                                   struct _linux_dirent64 *scratch, size_t scratch_size) {
    /* _index_worker_enumerate
     * Enumerate the supplied root entry, pushing the resultant entries into the supplied index context.
     *
     * @ struct ethread *thread            - The thread within which this enumeration is occuring.
     * @ struct index *index               - The index to push the index entries into.
     * @ struct index_root *root           - The root to enumerate.
     * @ struct _linux_direntx **entbuffer - The buffer to store read dirents in.
     * @ size_t *entbuffer_size            - The size of the dirents buffer.
     * @ struct _linux_dirent64 *scratch   - The scratch buffer.
     * @ size_t scratch_buffer             - The size of the scratch buffer.
     *
     * # int - 0 on success, -1 on error (hard), -2 on entry error (soft).
     */

    size_t entbuffer_read, iter;
    long scratch_read;
    struct _linux_dirent64 *dent;
    struct _linux_direntx *dir_ent;
    struct stat st;
    int ret;
    struct index_entry *entry;
    size_t root_deadlock_iter;

    root->dfd = openat(((root->parent != NULL) ? root->parent->dfd : AT_FDCWD),
                       root->path, (O_RDONLY | O_DIRECTORY | O_NOFOLLOW));
    if(root->dfd == -1) {
        debu("_index_worker_enumerate() failed: openat(%p, '%s') failed: %s", root->parent, root->path,
                                                                              strerror(errno));
        goto seqroot_and_errorent;
    }

    if(fstat(root->dfd, &root->st) == -1) {
        debu("_index_worker_enumerate() failed: fstat() failed: %s", strerror(errno));
        goto seqroot_and_errorent;
    }

    entbuffer_read = 0;
    while(1) {
        if(!ethread_running(thread)) {
            erro("_index_worker_enumerate() failed: Thread interrupted");
            goto error;
        }

        /* Fill the scratch buffer with real (_linux_dirent64) entries. */
        scratch_read = syscall(SYS_getdents64, root->dfd, scratch, scratch_size);
        if(scratch_read == -1) {
            erro("_index_worker_enumerate() failed: syscall(SYS_getdents64) failed: %s", strerror(errno));
            goto seqroot_and_errorent;
        }
        if(scratch_read == 0) {
            break;
        }

        /* Fill the entry buffer with virtual (_linux_direntx) entries (reallocating the entry buffer if required). */
        dent = scratch;
        while(scratch_read > 0) {
            strncpy((*entbuffer)[entbuffer_read].name, dent->d_name, NAME_MAX);

            scratch_read -= dent->d_reclen;
            dent = (((void *)dent) + dent->d_reclen);

            entbuffer_read++;
            if((entbuffer_read * sizeof(struct _linux_direntx)) >= (*entbuffer_size)) {
                warn("_index_worker_enumerate(): Entry buffer exhausted (%zu -> %zu)", (*entbuffer_size),
                                                                                       ((*entbuffer_size) * 2));
                (*entbuffer_size) *= 2;
                (*entbuffer) = realloc((*entbuffer), (*entbuffer_size));
                if((*entbuffer) == NULL) {
                    erro("_index_worker_enumerate() failed: realloc(%zu) failed: %s", (*entbuffer_size),
                                                                                      strerror(errno));
                    goto error;
                }
            }
        }
    }

    qsort((*entbuffer), entbuffer_read, sizeof(struct _linux_direntx), _index_worker_enumerate_sort);

    debu("[index] read %zu entries from '%s'", entbuffer_read, root->full_path);


    /* We need to wait until previous roots have been pushed into root/entry queues otherwise we may ruin our
     * lovely ordered entries.
     */
    if(_index_root_sequential_wait(thread, index, root->sequential_id) == -1) {
        goto error;
    }


    /* Fill index entries (index_entry) from the virtual entry buffer and push them into the index entry queue. */
    for(iter = 0; iter < entbuffer_read; iter++) {
        if(!ethread_running(thread)) {
            erro("_index_worker_enumerate() failed: Thread interrupted");
            goto error;
        }

        dir_ent = &(*entbuffer)[iter];

        /* Skip '.' and '..'. */
        if((strlen(dir_ent->name) == 1 && strncmp(dir_ent->name, ".", 1) == 0) ||
           (strlen(dir_ent->name) == 2 && strncmp(dir_ent->name, "..", 2) == 0)) {
            continue;
        }

        if(fstatat(root->dfd, dir_ent->name, &st, AT_SYMLINK_NOFOLLOW) == -1) {
            debu("_index_worker_enumerate() failed: fstatat('%s') failed: %s", dir_ent->name, strerror(errno));
            continue;
        }

        if(S_ISDIR(st.st_mode) && root->max_depth > 0) {
            root_deadlock_iter = 0;
            while(1) {
                if(!ethread_running(thread)) {
                    erro("_index_worker_enumerate() failed: Thread interrupted");
                    goto error;
                }

                ret = _index_root_add(index, root, dir_ent->name, (root->max_depth -1));
                if(ret == -1) {
                    erro("_index_worker_enumerate() failed: _index_root_add(%p, '%s') failed", root, dir_ent->name);
                    goto error;
                }
                if(ret == -2) {
                    root_deadlock_iter++;
                    if(root_deadlock_iter > 1e5) {
                        erro("_index_worker_enumerate() failed: Root queue deadlock detected (increase queue size)");
                        goto error;
                    }
                    usleep(10);
                    continue;
                }

                break;
            }
        }

        entry = _index_entry_alloc();
        if(entry == NULL) {
            erro("_index_worker_enumerate() failed: _index_entry_alloc() failed");
            goto error;
        }

        strncpy(entry->name, dir_ent->name, NAME_MAX);
        if(root->parent != NULL) {
            if(path_join(root->full_path, entry->name, entry->path, PATH_MAX) == -1) {
                erro("_index_worker_enumerate() failed: path_join('%s', '%s') failed", root->full_path, entry->name);
                goto free_entry_and_error;
            }
        } else {
            strncpy(entry->path, entry->name, PATH_MAX);
        }

        /* Mark the last entry in the root so we can fixup the root during sync. */
        if(iter == (entbuffer_read - 1)) {
            entry->last_in_root = true;
        } else {
            entry->last_in_root = false;
        }

        memcpy(&entry->st, &st, sizeof(struct stat));

        entry->root = root;
        if(eref_acquire(&entry->root->reference) == -1) {
            erro("_index_worker_enumerate() failed: eref_acquire() failed");
            goto free_entry_and_error;
        }

        debu("[index] add entry '%s'", entry->path);
        if(_index_entry_blocking_push(thread, index->entry_queue, entry) == -1) {
            erro("_index_worker_enumerate() failed: _index_entry_blocking_push() failed");
            goto release_root_free_entry_and_error;
        }
        econd_broadcast(index->next_cond);
    }

    eatomic_add(&index->root_sequential_done, 1);

    return 0;

    release_root_free_entry_and_error:
        eref_release(&root->reference);
    free_entry_and_error:
        index_entry_destroy(entry);
    error:
        return -1;

    seqroot_and_errorent:
        if(_index_root_sequential_wait(thread, index, root->sequential_id) == -1) {
            goto error;
        }
        eatomic_add(&index->root_sequential_done, 1);

        return -2;
}

static void _index_worker(struct ethread *thread) {
    struct index *index = (struct index *)thread->user_data;
    bool active;
    struct _linux_direntx *entbuffer;
    struct _linux_dirent64 *scratch;
    size_t entbuffer_size, scratch_size;
    struct index_root *root;
    int ret;

    ethread_running_signal(thread);

    /* These scratch buffers are used to pull dirents in from the kernel before they're pushed into the entry queue.
     * The initial size isn't particularly important.  If required _worker_enumerate() will reallocate it.
     */
    entbuffer_size = (sysconf(_SC_PAGE_SIZE) * sysconf(_SC_PAGE_SIZE));
    entbuffer = malloc(entbuffer_size);
    if(entbuffer == NULL) {
        erro("_index_worker() failed: malloc(%zu) failed: %s", entbuffer_size, strerror(errno));
        goto error;
    }

    scratch_size = (sysconf(_SC_PAGE_SIZE) * sysconf(_SC_PAGE_SIZE));
    scratch = malloc(scratch_size);
    if(scratch == NULL) {
        erro("_index_worker() failed: malloc(%zu) failed: %s", scratch_size, strerror(errno));
        goto free_entbuffer_and_error;
    }

    active = true;
    while(ethread_running(thread)) {
        if(eatomic_load(&index->workers_failed) != 0) {
            erro("_index_worker() failed: One or more worker threads failed");
            break;
        }

        if(equeue_pop(index->root_queue, (void **)&root) == -1) {
            if(active) {
                active = false;
                eatomic_sub(&index->workers_active, 1);
                debu("[index] worker finished");
            }

            equeue_wait(index->root_queue, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e6});
            continue;
        }
        if(!active) {
            active = true;
            eatomic_add(&index->workers_active, 1);
        }

        ret = _index_worker_enumerate(thread, index, root,
                                      &entbuffer, &entbuffer_size,
                                      scratch, scratch_size);
        if(ret == -1) {
            erro("_index_worker() failed: _index_worker_enumerate(%p, %p) failed", index, root);
            goto free_scratch_entbuffer_and_error;
        }
        if(ret == -2) {
            debu("_index_worker() failed: _index_worker_enumerate('%s') failed", root->full_path);
        }

        eref_release(&root->reference);
    }

    free(scratch);
    free(entbuffer);
    end:
        ethread_joinable_signal(thread);
        return;

    free_scratch_entbuffer_and_error:
        free(scratch);
    free_entbuffer_and_error:
        free(entbuffer);
    error:
        eatomic_add(&index->workers_failed, 1);
        goto end;
}


static void _index_delta_worker(struct ethread *thread);

struct index_delta *index_delta_create(struct index *alpha, struct index *beta, size_t queue_size,
                                       struct econd *last_cond, struct econd *next_cond,
                                       bool inter_check_size,
                                       bool inter_check_mtime, struct timespec inter_mtime_window,
                                       enum index_delta_inter_method inter_method,
                                       enum index_delta_inter_auto_pref inter_auto_pref) {
    /* index_delta_create
     * Create a new index delta, calculating the intersection and compliments of the supplied indexes.
     * The indexes may be fully or partially complete.
     * The indexes must not be destroyed before the delta.
     *
     * @ struct index *alpha                - The first index to compare.
     * @ struct index *beta                 - The second index to compare.
     * @ size_t queue_size                  - The size of the output queue(s).
     * @ struct econd *last_cond            - The cond that the last item in the pipeline will signal.
     * @ struct econd *next_cond            - The cond to signal the next item in the pipeline.
     * @ bool inter_check_size              - True if the entry size should be used to resolve intersections.
     * @ bool inter_check_mtime             - True if the entry mtime should be used to resolve intersections.
     * @ struct timespec inter_mtime_window - The window which entry mtimes must be within to match.
     * @ enum index_delta_inter_method      - The method by which intersections are resolved.
     * @ enum index_delta_auto_pref         - The preference for inter auto resolution.
     *
     * # struct index_delta *delta - The newly created delta, or NULL on error.
     */

    struct index_delta *delta;

    delta = malloc(sizeof(struct index_delta));
    if(delta == NULL) {
        erro("index_delta_create(%p, %p) failed: malloc(%zu) failed: %s", alpha, beta, sizeof(struct index_delta),
                                                                          strerror(errno));
        goto error;
    }

    delta->index_alpha = alpha;
    delta->index_beta = beta;

    delta->inter_check_size = inter_check_size;
    delta->inter_check_mtime = inter_check_mtime;
    delta->inter_mtime_window = inter_mtime_window;
    delta->inter_method = inter_method;
    delta->inter_auto_pref = inter_auto_pref;

    delta->intersect_queue = equeue_create(queue_size, 1, 1);
    delta->comp_alpha_queue = equeue_create(queue_size, 1, 1);
    delta->comp_beta_queue = equeue_create(queue_size, 1, 1);

    delta->worker_active = true;
    delta->worker_failed = false;

    delta->last_cond = last_cond;
    delta->next_cond = next_cond;

    delta->thread = ethread_create(_index_delta_worker, "index_delta", (void *)delta);

    return delta;

    error:
        return NULL;
}

void index_delta_destroy(struct index_delta *delta) {
    /* index_delta_destroy
     * Destroy the supplied index delta.
     *
     * @ struct index_delta *delta - The index delta to destroy.
     */

    struct index_entry *entry;

    ethread_destroy_signal(delta->thread, delta->last_cond);

    while(equeue_get_size(delta->intersect_queue) > 0) {
        if(equeue_pop(delta->intersect_queue, (void **)&entry) == 0) {
            index_entry_destroy(entry);
        }
    }
    equeue_destroy(delta->intersect_queue);

    while(equeue_get_size(delta->comp_alpha_queue) > 0) {
        if(equeue_pop(delta->comp_alpha_queue, (void **)&entry) == 0) {
            index_entry_destroy(entry);
        }
    }
    equeue_destroy(delta->comp_alpha_queue);

    while(equeue_get_size(delta->comp_beta_queue) > 0) {
        if(equeue_pop(delta->comp_beta_queue, (void **)&entry) == 0) {
            index_entry_destroy(entry);
        }
    }
    equeue_destroy(delta->comp_beta_queue);

    free(delta);
}


static int _index_delta_pop(struct index_delta *delta, struct equeue *queue, struct index_entry **entry) {
    /* _index_delta_pop
     * Pop an entry from the supplied delta's queue.
     * The caller must later destroy the entry using index_entry_destroy().
     * Note that a return value of 1 indicating that the queue is empty does not mean that the delta is finished.
     *
     * @ struct index_delta *delta  - The delta that the entry is being popped off of.
     * @ struct equeue *queue       - The delta queue to pop the entry off of.
     * @ struct index_entry **entry - A pointer to an index_entry ptr to store the entry ptr in.
     *
     * # int - 0 on success, 1 if the queue is (currently) empty, 2 if the delta is finished,
     *         -1 if one or more worker threads have failed.
     */

    if(eatomic_load(&delta->worker_failed)) {
        goto error;
    }

    if(equeue_pop(queue, (void **)entry) == -1) {
        if(!eatomic_load(&delta->worker_active)) {
            return 2;
        }

        return 1;
    }

    return 0;

    error:
        return -1;
}

int index_delta_intersect_pop(struct index_delta *delta, struct index_entry **entry) {
    /* index_delta_intersect_pop
     * See _index_delta_pop().
     */

    return _index_delta_pop(delta, delta->intersect_queue, entry);
}

int index_delta_comp_alpha_pop(struct index_delta *delta, struct index_entry **entry) {
    /* index_delta_comp_alpha_pop
     * See _index_delta_pop().
     */

    return _index_delta_pop(delta, delta->comp_alpha_queue, entry);
}

int index_delta_comp_beta_pop(struct index_delta *delta, struct index_entry **entry) {
    /* index_delta_comp_beta_pop
     * See _index_delta_pop().
     */

    return _index_delta_pop(delta, delta->comp_beta_queue, entry);
}

void index_delta_intersect_wait(struct index_delta *delta, struct timespec *timeout) {
    equeue_wait(delta->intersect_queue, timeout);
}

void index_delta_comp_alpha_wait(struct index_delta *delta, struct timespec *timeout) {
    equeue_wait(delta->comp_alpha_queue, timeout);
}

void index_delta_comp_beta_wait(struct index_delta *delta, struct timespec *timeout) {
    equeue_wait(delta->comp_beta_queue, timeout);
}


static int _index_delta_worker_entry_pop(struct index *index, struct index_entry **entry) {
    /* _index_delta_worker_entry_pop
     * Pop an entry from the supplied index.
     * Note that the returned entry will be NULL if the index is finished and empty.
     *
     * @ struct index *index        - The index to pop the entry off of.
     * @ struct index_entry **entry - A pointer to an index_entry ptr to store the entry in.
     *
     * # int - 0 on success, -1 on error.
     */

    int ret;

    *entry = NULL;
    while(*entry == NULL) {
        ret = index_entry_pop(index, entry);
        if(ret == -1) {
            goto error;
        }
        if(ret == 1) {
            index_entry_wait(index, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e6});
            continue;
        }

        /* 0 (entry now filled) or 2 indexing finished (entry NULL). */
        break;
    }

    return 0;

    error:
        return -1;
}

static bool _index_delta_worker_entry_inter_check(struct index_delta *delta,
                                                  struct index_entry *entry_alpha,
                                                       struct index_entry *entry_beta) {
    size_t delta_size;
    struct timespec delta_time;

    if(!delta->inter_check_size && !delta->inter_check_mtime) {
        debu("[delta] inter entry '%s' no configured checks", entry_alpha->path);
        return false;
    }

    delta_size = (entry_alpha->st.st_size - entry_beta->st.st_size);
    _etime_delta(&entry_alpha->st.st_mtim, &entry_beta->st.st_mtim, &delta_time);

    if(delta->inter_check_size && delta_size != 0) {
        debu("[delta] inter entry '%s' mismatch size", entry_alpha->path);
        return false;
    }

    if(delta->inter_check_mtime && _etime_delta(&delta_time, &delta->inter_mtime_window, NULL) == -1) {
        debu("[delta] inter entry '%s' mismatch mtime", entry_alpha->path);
        return false;
    }

    debu("[delta] inter entry '%s' pass checks", entry_alpha->path);
    return true;
}

static int _index_delta_worker_entry_inter(struct index_delta *delta,
                                           struct index_entry *entry_alpha, struct index_entry *entry_beta) {
    /* _index_delta_worker_entry_inter
     * Resolve the intersection between the supplied entries.
     *
     * @ struct index_delta *delta       - The delta in which the entries are being compared.
     * @ struct index_entry *entry_alpha - The first entry to compare.
     * @ struct index_entry *entry_beta  - The second entry to compare.
     *
     * # int - 0 if the entries are the same, less than 0 if alpha is less than beta, and greater than 0 if
     *         alpha is greater than beta.
     */

    int time_dir;

    if(_index_delta_worker_entry_inter_check(delta, entry_alpha, entry_beta)) {
        return 0;
    }

    switch(delta->inter_method) {
        case INDEX_DELTA_INTER_NONE:
            debu("[delta] entry '%s' inter none", entry_alpha->path);
            return 0;

        case INDEX_DELTA_INTER_LEFT:
            debu("[delta] entry '%s' inter left", entry_alpha->path);
            return -1;

        case INDEX_DELTA_INTER_RIGHT:
            debu("[delta] entry '%s' inter right", entry_alpha->path);
            return 1;

        case INDEX_DELTA_INTER_AUTO:
            switch(delta->inter_auto_pref) {
                case INDEX_DELTA_INTER_AUTO_NONE:
                    debu("[delta] entry '%s' inter auto none", entry_alpha->path);
                    return 0;

                case INDEX_DELTA_INTER_AUTO_LEFT:
                    debu("[delta] entry '%s' inter auto left", entry_alpha->path);
                    return -1;

                case INDEX_DELTA_INTER_AUTO_RIGHT:
                    debu("[delta] entry '%s' inter auto right", entry_alpha->path);
                    return 1;

                case INDEX_DELTA_INTER_AUTO_TIME:
                    time_dir = _etime_delta(&entry_alpha->st.st_mtim, &entry_beta->st.st_mtim, NULL);
                    debu("[delta] entry '%s' inter auto time (%d)", entry_alpha->path, time_dir);
                    return time_dir;
            };
    };
}

static int _index_delta_worker_entry_comp(struct index_entry *entry_alpha, struct index_entry *entry_beta) {
    /* _index_delta_worker_entry_comp
     * Compare the supplied entries, either of which may be NULL.
     *
     * @ struct index_entry *entry_alpha - The first entry to compare.
     * @ struct index_entry *entry_beta  - The second entry to compare.
     *
     * # int - 0 if the entries are the same, less than 0 if alpha is less than beta, and greater than 0 if
               alpha is greater than beta.
     */

    if(entry_alpha == NULL) {
        return 1;
    }
    if(entry_beta == NULL) {
        return -1;
    }

    /* Entries are enumerated breadth and only sorted locally so if we have just crossed a depth boundry
     * we need to favor the entry with the lowest depth relative to the root (e.g. highest max depth).
     */
    if(       entry_alpha->root->max_depth > entry_beta->root->max_depth) {
        return -1;
    } else if(entry_alpha->root->max_depth < entry_beta->root->max_depth) {
        return 1;
    }

    return strcmp(entry_alpha->path, entry_beta->path);
}

static void _index_delta_worker(struct ethread *thread) {
    struct index_delta *delta = (struct index_delta *)thread->user_data;
    struct index_entry *entry_alpha, *entry_beta;
    int ret;

    ethread_running_signal(thread);

    entry_alpha = NULL; entry_beta = NULL;
    while(ethread_running(thread)) {
        /* Pull entries in from the indexes. */
        if(entry_alpha == NULL) {
            if(_index_delta_worker_entry_pop(delta->index_alpha, &entry_alpha) == -1) {
                erro("_index_delta_worker() failed: _index_delta_worker_entry_pop() failed");
                goto free_entries_and_error;
            }
        }

        if(entry_beta == NULL) {
            if(_index_delta_worker_entry_pop(delta->index_beta, &entry_beta) == -1) {
                erro("_index_delta_worker() failed: _index_delta_worker_entry_pop() failed");
                goto free_entries_and_error;
            }
        }

        if(entry_alpha == NULL && entry_beta == NULL) {
            if(eatomic_load(&delta->worker_active)) {
                eatomic_store(&delta->worker_active, false);
                debu("[delta] worker finished");
            }

            econd_wait(delta->last_cond, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e6});
            continue;
        }
        eatomic_store(&delta->worker_active, true);


        /* Compare entries. */
        ret = _index_delta_worker_entry_comp(entry_alpha, entry_beta);
        if(ret == 0) { /* Entries are the same, resolve intersect. */
            ret = _index_delta_worker_entry_inter(delta, entry_alpha, entry_beta);
            if(ret == 0) { /* Entries intersect the same, push one into the intersect queue. */
                debu("[delta] entry '%s' intersect", entry_alpha->path);
                if(_index_entry_blocking_push(thread, delta->intersect_queue, entry_alpha) == -1) {
                    erro("_index_delta_worker() failed: _index_entry_blocking_push() failed");
                    goto free_entries_and_error;
                }

                index_entry_destroy(entry_beta);
            } else if(ret < 0) { /* Entries intersect in favor of alpha. */
                debu("[delta] entry '%s' compliment alpha (intersect)", entry_alpha->path);
                if(_index_entry_blocking_push(thread, delta->comp_alpha_queue, entry_alpha) == -1) {
                    erro("_index_delta_worker() failed: _index_entry_blocking_push() failed");
                    goto free_entries_and_error;
                }

                index_entry_destroy(entry_beta);
            } else if(ret > 0) { /* Entries intersect in favor of beta. */
                debu("[delta] entry '%s' compliment beta (intersect)", entry_beta->path);
                if(_index_entry_blocking_push(thread, delta->comp_beta_queue, entry_beta) == -1) {
                    erro("_index_delta_worker() failed: _index_entry_blocking_push() failed");
                    goto free_entries_and_error;
                }

                index_entry_destroy(entry_alpha);
            }

            entry_alpha = NULL; entry_beta = NULL;
        } else if(ret < 0) { /* Alpha is less than beta (or beta is NULL), push alpha into it's complement queue. */
            debu("[delta] entry '%s' compliment alpha", entry_alpha->path);
            if(_index_entry_blocking_push(thread, delta->comp_alpha_queue, entry_alpha) == -1) {
                erro("_index_delta_worker() failed: _index_entry_blocking_push() failed");
                goto free_entries_and_error;
            }

            entry_alpha = NULL;
        } else if(ret > 0) { /* Beta is less than alpha (or alpha is NULL), push beta into it's complement queue. */
            debu("[delta] entry '%s' compliment beta", entry_beta->path);
            if(_index_entry_blocking_push(thread, delta->comp_beta_queue, entry_beta) == -1) {
                erro("_index_delta_worker() failed: _index_entry_blocking_push() failed");
                goto free_entries_and_error;
            }

            entry_beta = NULL;
        }

        econd_broadcast(delta->next_cond);
    }

    end:
        ethread_joinable_signal(thread);
        return;

    free_entries_and_error:
        if(entry_alpha != NULL) {
            index_entry_destroy(entry_alpha);
        }
        if(entry_beta != NULL) {
            index_entry_destroy(entry_beta);
        }

        eatomic_store(&delta->worker_failed, true);
        goto end;
}
