#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <syslog.h>


/* Macros. */
#define debu(format, ...) _log_internal(LOG_DEBUG,   format "\n", ##__VA_ARGS__)
#define info(format, ...) _log_internal(LOG_INFO,    format "\n", ##__VA_ARGS__)
#define warn(format, ...) _log_internal(LOG_WARNING, format "\n", ##__VA_ARGS__)
#define erro(format, ...) _log_internal(LOG_ERR,     format "\n", ##__VA_ARGS__)


struct log_context {
    int log_level;
};


/* Function prototypes. */
void _log_internal(int priority, const char *format, ...);

void log_level_set(int priority);

#endif
