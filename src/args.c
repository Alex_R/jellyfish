#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>
#include <locale.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>
#include <syslog.h>

#include "args.h"
#include "main.h"


static void _args_command_version(void) {
    /* _args_command_version
     * Write basic version information to stdout.
     */

    fprintf(stdout,
        MAIN_FULL_VERSION"\n"
    );
}

static void _args_command_usage(void) {
    /* _args_command_usage
     * Write basic usage information to stdout.
     */

    fprintf(stdout,
        MAIN_FULL_VERSION"\n"
        MAIN_COPYRIGHT"\n"
        "Usage: "MAIN_NAME" [options] left_root right_root\n"
        "Try '"MAIN_NAME" --help' for more information.\n"
    );
}

static void _args_command_help(void) {
    /* _args_command_help
     * Write detailed help information to stdout.
     */

    fprintf(stdout,
        MAIN_FULL_VERSION"\n"
        MAIN_COPYRIGHT"\n"
        "\n"
        "Usage:\n"
        "  "MAIN_NAME" [options] left_root right_root - Synchronise files between the left and right roots.\n"
        "\n"
        "General Options:\n"
        "  -v, --version   - Display basic version information.\n"
        "  -u, --usage     - Display basic usage information.\n"
        "  -h, --help      - Display this help information.\n"
        "  -l, --log_level - Set the output log level.  Either debug, info, warning, error, or none (default info).\n"
        "\n"
        "Index Options\n"
        "  -d, --index_max_depth     - The maximum depth to recurse to while indexing the left and right side.\n"
        "                              0 to disable recursion, -1 for no limit (the default).\n"
        "  -p, --index_pipeline_size - The total size of the combined queues in the index pipeline (default 2^20).\n"
        "  -i, --index_left_threads  - The number of worker threads to spawn for the left index (default 1).\n"
        "  -I, --index_right_threads - The number of worker threads to spawn for the right index (default 1).\n"
        "  -s, --index_check_size    - True to check for size delta between entries with the same path, False.\n"
        "                              otherwise (default true).\n"
        "  -m, --index_check_mtime   - True to check for modification time delta between entries with the same path\n"
        "                              False otherwise (default true).\n"
        "  -M, --index_mtime_window  - The delta window in miliseconds within which the index mtime check\n"
        "                              considers entries equal (default 0).\n"
        "  -r, --index_inter_resolve - The index intersect resolution method (default auto, see below).  Optionally\n"
        "                              'left', 'right', or 'none' to force intersect resolution towards the left\n"
        "                              side, right side, or intersect respectively.\n"
        "\n"
        "  Index Intersect Resolution\n"
        "    If two index entries have the same path they are run against the size and mtime checks (if configured).\n"
        "    If they fail the checks (or neither check is configured) they require resolution.  This can be forced\n"
        "    to one side or the other using the index_inter_resolve option but the default is auto as below.\n"
        "\n"
        "    Automatic index intersect resolution is based on the configured sync actions.\n"
        "      - If one side is COPY and the other is another action (NONE or DELETE) then the intersect is resolved\n"
        "        in favor of the COPY side.\n"
        "      - If both sides are COPY then the intersect is resolved in favor of the newest side per mtime.\n"
        "      - Otherwise (NONE, NONE or DELETE, DELETE) they intersect regardless of the size/time mismatch.\n"
        "\n"
        "Sync Options\n"
        "  -w, --sync_left_threads  - The number of worker threads to spawn for the left side sync (default 1).\n"
        "  -W, --sync_right_threads - The number of worker threads to spawn for the right side sync (default 1).\n"
        "  -P, --sync_pipeline_size - The total size of the combined queues in the sync pipeline (default 2^10).\n"
        "  -L, --sync_left_action   - The action to perform to resolve left side synchronisation (default copy).\n"
        "  -R, --sync_right_action  - The action to perform to resolve right side synchronisation (default none).\n"
        "  -e, --sync_left_delay    - The time in miliseconds that left sync target file atimes must be older than\n"
        "                             (default 0).\n"
        "  -E, --sync_right_delay   - The time in miliseconds that right sync target file atimes must be older than.\n"
        "                             (default 0).\n"
        "  -o, --sync_status_output - The format to output sync status reports in (default none).\n"
        "\n"
        "  Sync Actions\n"
        "    none   - Perform no action.\n"
        "    copy   - Copy the file.\n"
        "    delete - Delete the file.\n"
        "\n"
        "  Sync Output Formats\n"
        "    none  - No output.\n"
        "    hline - One line per entry.  --> for successful right sync, <-- for successful left sync,\n"
        "            --- for intersection, and !!> <!! for an error on each side respectively.\n"
        "    mline - Double-null-separated entries.  3 single-null-separated items per entry, direction (0 for,\n"
        "               left, 1 for right, 2 for intersect), status code (0 or negative errno), and path.\n"
        "    msum  - Double-null-separated entries.  7 single-null-separated items per entry, entries synced left,\n"
        "            entries failed left, bytes synced left, entries synced right, entries failed right, bytes\n"
        "            synced right, entries intersected.\n"
    );
}

static struct option _args_command_options[] = {
    {"version",   no_argument,       NULL, 'v'},
    {"usage",     no_argument,       NULL, 'u'},
    {"help",      no_argument,       NULL, 'h'},
    {"log_level", required_argument, NULL, 'l'},

    {"index_max_depth",     required_argument, NULL, 'd'},
    {"index_pipeline_size", required_argument, NULL, 'i'},
    {"index_right_threads", required_argument, NULL, 'i'},
    {"index_left_threads",  required_argument, NULL, 'I'},
    {"index_check_size",    required_argument, NULL, 's'},
    {"index_check_mtime",   required_argument, NULL, 'm'},
    {"index_mtime_window",  required_argument, NULL, 'M'},
    {"index_inter_resolve", required_argument, NULL, 'r'},

    {"sync_left_threads",  required_argument, NULL, 'w'},
    {"sync_right_threads", required_argument, NULL, 'W'},
    {"sync_pipeline_size", required_argument, NULL, 'P'},

    {"sync_left_action",  required_argument, NULL, 'L'},
    {"sync_right_action", required_argument, NULL, 'R'},

    {"sync_left_delay",  required_argument, NULL, 'e'},
    {"sync_right_delay", required_argument, NULL, 'E'},

    {"sync_status_output", required_argument, NULL, 'o'},

    {NULL, 0, NULL, 0},
};

static const char *_args_command_options_string = "vuhdpiIsmMrwWPLRoeE";

static struct args _args_default_arguments = {
    .log_level = LOG_INFO,

    .index_max_depth = -1,
    .index_pipeline_size = (1 << 20),
    .index_right_threads = 1,
    .index_left_threads = 1,
    .index_check_size = true,
    .index_check_mtime = true,
    .index_mtime_window = {.tv_sec = 0, .tv_nsec = 0},
    .index_inter_resolve = INDEX_DELTA_INTER_AUTO,

    .sync_left_threads = 1,
    .sync_right_threads = 1,
    .sync_pipeline_size = (1 << 10),

    .sync_left_action = SYNC_ACTION_COPY,
    .sync_right_action = SYNC_ACTION_NONE,

    .sync_left_delay  = {.tv_sec = 0, .tv_nsec = 0},
    .sync_right_delay = {.tv_sec = 0, .tv_nsec = 0},

    .sync_status_output = MAIN_STATUS_FMT_NONE,
};


static int _args_parse_option_sizet(const char *name, const char *arg, size_t *value, size_t min, size_t max) {
    errno = 0;
    *value = strtoull(arg, NULL, 10);
    if(errno != 0) {
        fprintf(stderr, MAIN_NAME" supplied '%s' is not an integer\n", name);
        _args_command_usage();
        goto error;
    }
    if(*value < min || *value > max) {
        fprintf(stderr, MAIN_NAME" supplied '%s' must be between '%zd' and '%zd'\n", name, min, max);
        _args_command_usage();
        goto error;
    }

    return 0;

    error:
        return -1;
}

static int _args_parse_option_bool(const char *name, const char *arg, bool *value) {
    if((strlen(arg) == 4 && strncasecmp(arg, "true", 4) == 0) ||
       (strlen(arg) == 3 && strncasecmp(arg, "yes",  3) == 0)) {
        *value = true;
        return 0;
    }

    if((strlen(arg) == 5 && strncasecmp(arg, "false", 5) == 0) ||
       (strlen(arg) == 2 && strncasecmp(arg, "no",    2) == 0)) {
        *value = false;
        return 0;
    }

    fprintf(stderr, MAIN_NAME" supplied '%s' must be 'true' or 'false'\n", name);
    return -1;
}

struct _args_option_map {
    const char *key;
    int value;
};

static int _args_parse_option_map(const char *name, const char *arg, int *value, struct _args_option_map *map) {
    size_t iter;

    if(arg != NULL) {
        for(iter = 0; map[iter].key != NULL; iter++) {
            if(strlen(arg) == strlen(map[iter].key) && strncasecmp(arg, map[iter].key, strlen(map[iter].key)) == 0) {
                *value = map[iter].value;
                return 0;
            }
        }
    }

    fprintf(stderr, MAIN_NAME" supplied '%s' is not valid\n", name);
    return -1;
}

static int _args_parse_option(int key, const char *arg, struct args *arguments) {
    /* _args_parse_opt
     * Parse an option with the supplied key code and option index.
     *
     * @ int key                - The key code for the option to parse, or a special ARGS_* value.
     * @ char *arg              - The argument for the option, if appropriate.
     * @ struct args *arguments - The arguments structure to update with the parsed option.
     *
     * # int - 0 on success, 1 to stop parsing, -1 on error.
     */

    size_t value;

    switch(key) {
        case 'v':
            _args_command_version();
            return 1;

        case 'u':
            _args_command_usage();
            return 1;

        case 'h':
            _args_command_help();
            return 1;

        case 'l':
            return _args_parse_option_map("log_level", arg, &arguments->log_level, (struct _args_option_map []){
                {"debug", LOG_DEBUG},
                {"info", LOG_INFO},
                {"warning", LOG_WARNING},
                {"error", LOG_ERR},
                {"none", LOG_EMERG},

                {NULL, 0},
            });


        case 'd':
            return _args_parse_option_sizet("index_max_depth", arg, &arguments->index_max_depth,
                                                                    -1, SIZE_MAX);

        case 'p':
            return _args_parse_option_sizet("index_pipeline_size", arg, &arguments->index_pipeline_size,
                                                                        0, SIZE_MAX);

        case 'i':
            return _args_parse_option_sizet("index_right_threads", arg, &arguments->index_right_threads,
                                                                        0, SIZE_MAX);

        case 'I':
            return _args_parse_option_sizet("index_left_threads", arg, &arguments->index_left_threads,
                                                                       0, SIZE_MAX);

        case 's':
            return _args_parse_option_bool("index_check_size", arg, &arguments->index_check_size);

        case 'm':
            return _args_parse_option_bool("index_check_mtime", arg, &arguments->index_check_mtime);

        case 'M':
            if(_args_parse_option_sizet("index_mtime_window", arg, &value, 0, SIZE_MAX) == -1) {
                return -1;
            }
            arguments->index_mtime_window.tv_sec = (value / 1e3);
            arguments->index_mtime_window.tv_nsec = ((value * 1e6) - (arguments->index_mtime_window.tv_sec * 1e9));
            return 0;

        case 'r':
            return _args_parse_option_map("index_inter_resolve", arg, (int *)&arguments->index_inter_resolve,
                                                                      (struct _args_option_map []){
                {"none",  INDEX_DELTA_INTER_NONE},
                {"left",  INDEX_DELTA_INTER_LEFT},
                {"right", INDEX_DELTA_INTER_RIGHT},
                {"auto",  INDEX_DELTA_INTER_AUTO},

                {NULL, 0},
            });


        case 'w':
            return _args_parse_option_sizet("sync_left_threads", arg, &arguments->sync_left_threads, 0, SIZE_MAX);

        case 'W':
            return _args_parse_option_sizet("sync_right_threads", arg, &arguments->sync_right_threads, 0, SIZE_MAX);

        case 'P':
            return _args_parse_option_sizet("sync_pipeline_size", arg, &arguments->sync_pipeline_size, 0, SIZE_MAX);

        case 'L':
            return _args_parse_option_map("sync_left_action", arg, (int *)&arguments->sync_left_action,
                                                                   (struct _args_option_map []){
                {"none",   SYNC_ACTION_NONE},
                {"copy",   SYNC_ACTION_COPY},
                {"delete", SYNC_ACTION_DELETE},

                {NULL, 0},
            });

        case 'R':
            return _args_parse_option_map("sync_right_action", arg, (int *)&arguments->sync_right_action,
                                                                    (struct _args_option_map []){
                {"none",   SYNC_ACTION_NONE},
                {"copy",   SYNC_ACTION_COPY},
                {"delete", SYNC_ACTION_DELETE},

                {NULL, 0},
            });

        case 'e':
            if(_args_parse_option_sizet("sync_left_delay", arg, &value, 0, SIZE_MAX) == -1) {
                return -1;
            }
            arguments->sync_left_delay.tv_sec = (value / 1e3);
            arguments->sync_left_delay.tv_nsec = ((value * 1e6) - (arguments->sync_left_delay.tv_sec * 1e9));
            return 0;

        case 'E':
            if(_args_parse_option_sizet("sync_right_delay", arg, &value, 0, SIZE_MAX) == -1) {
                return -1;
            }
            arguments->sync_right_delay.tv_sec = (value / 1e3);
            arguments->sync_right_delay.tv_nsec = ((value * 1e6) - (arguments->sync_right_delay.tv_sec * 1e9));
            return 0;

        case 'o':
            return _args_parse_option_map("sync_status_output", arg, (int *)&arguments->sync_status_output,
                                                                     (struct _args_option_map []){
                {"none",  MAIN_STATUS_FMT_NONE},
                {"hline", MAIN_STATUS_FMT_HLINE},
                {"mline", MAIN_STATUS_FMT_MLINE},
                {"msum",  MAIN_STATUS_FMT_MSUM},

                {NULL, 0},
            });


        case ARGS_EXTRA:
            if(arguments->left_root_path[0] == '\0') {
                strncpy(arguments->left_root_path, arg, PATH_MAX);
            } else if(arguments->right_root_path[0] == '\0') {
                strncpy(arguments->right_root_path, arg, PATH_MAX);
            } else {
                fprintf(stderr, MAIN_NAME" more than 2 roots supplied.\n");
                _args_command_usage();
                return -1;
            }
            return 0;

        case ARGS_END:
            if(arguments->left_root_path[0] == '\0' || arguments->right_root_path[0] == '\0') {
                fprintf(stderr, MAIN_NAME" not enough roots supplied.\n");
                _args_command_usage();
                return -1;
            }
            return 0;

        default:
            _args_command_usage();
            return -1;
    };
}


struct args *args_parse(int argc, char *argv[]) {
    /* args_parse
     * Parse the arguments in the supplied standard format argc and argv.
     * Note: 'optarg' and 'optind' are globals exported by getopt.
     *
     * @ int argc   - The number of elements to parse from the argv structure.
     * @ char *argv - The command line elements.
     *
     * # struct args * - A newly allocated args structure containing the parsed arguments, or NULL if parsing fails.
     */

    struct args *arguments;
    int key, ret, iter;

    arguments = malloc(sizeof(struct args));
    if(arguments == NULL) {
        fprintf(stderr, MAIN_NAME" args_parse(%d, %p) failed: malloc(%ld) failed: %s", argc, argv, sizeof(struct args), strerror(errno));
    }

    memcpy(arguments, &_args_default_arguments, sizeof(struct args));


    if(setlocale(LC_ALL, "") == NULL) {
        fprintf(stderr, MAIN_NAME" args_parse(%d, %p) failed: setlocale() failed: %s", argc, argv, strerror(errno));
    }

    /* Parse structured options. */
    while(1) {
        key = getopt_long(argc, argv, _args_command_options_string, _args_command_options, NULL);
        if(key == -1) {
            break;
        }

        ret = _args_parse_option(key, optarg, arguments);
        if(ret == -1) {
            goto error;
        }
        if(ret == 1) {
            goto done;
        }
    }

    /* Parse extra arguments. */
    for(iter = optind; iter < argc; iter++) {
        ret = _args_parse_option(ARGS_EXTRA, argv[iter], arguments);
        if(ret == -1) {
            goto error;
        }
        if(ret == 1) {
            goto done;
        }
    }

    ret = _args_parse_option(ARGS_END, NULL, arguments);
    if(ret == -1) {
        goto error;
    }

    done:
        return arguments;

    error:
        args_destroy(arguments);
        return NULL;
}

void args_destroy(struct args *arguments) {
    /* args_destroy
     * Destroy the supplied args structure (previously allocated by args_parse()).
     *
     * @ struct args *arguments - The args structure to destroy.
     */

    free(arguments);
}
