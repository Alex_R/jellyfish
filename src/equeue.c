#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "log.h"
#include "die.h"
#include "equeue.h"
#include "eatomic.h"
#include "econd.h"
#include "ethread.h"


static struct equeue_node *_equeue_node_create(void *node_region, size_t index) {
    /* _equeue_node_create
     * Allocate and initialise a new queue node.
     *
     * # struct equeue_node * - The newly created queue node.
     */

    return node_region + (sizeof(struct equeue_node) * index);
}


static void _equeue_create_add_node(struct equeue *queue, size_t index) {
    /* _equeue_create_add_node
     * Add a new node to the supplied equeue.
     *
     * @ struct equeue *queue - The queue to create the node in.
     * @ size_t index         - The index of the node being created in the queue's node region.
     */

    struct equeue_node *node;

    node = _equeue_node_create(queue->node_region, index);

    node->next = NULL;

    if(queue->tail == NULL) {
        queue->head = node;
        queue->tail = node;
    } else {
        queue->tail->next = node;
        queue->tail = node;
    }

    node->data = NULL;
    node->lock = false;
}

static void _equeue_create_block_links(struct equeue *queue) {
    /* _equeue_create_block_links
     * Create the equeue block links for the supplied queue.
     *
     * @ struct equeue *queue - The queue to create the block links for.
     */

    size_t iter;
    struct equeue_node *node, *next_block;

    if((queue->max_size % queue->block_size) != 0) {
        die("_equeue_create_block_links(%p) failed: Block size is not a factor of the queue size", queue);
    }

    node = queue->head;
    next_block = node;
    for(iter = 0; iter < queue->block_size; iter++) {
        next_block = next_block->next;
    }

    for(iter = 0; iter < queue->max_size; iter++) {
        node->next_block = next_block;

        node = node->next;
        next_block = next_block->next;
    }
}

struct equeue *equeue_create(size_t max_size, size_t block_size, size_t threads) {
    /* equeue_create
     * Create a new equeue using the supplied max size and threads.
     *
     * @ size_t max_size   - The maximum size of the queue.
     * @ size_t block_size - The number of nodes in a block in the queue.
     * @ size_t threads    - The number of threads that will be using the queue.
     *
     * # struct equeue * - The newly created equeue.
     */

    struct equeue *queue;
    size_t iter;

    queue = malloc(sizeof(struct equeue));
    if(queue == NULL) {
        die("equeue_create(%ld, %ld, %ld) failed: malloc(%ld) failed: %s", max_size, block_size, threads,
                                                                            sizeof(struct equeue), strerror(errno));
    }

    queue->node_region = malloc(sizeof(struct equeue_node) * max_size);
    if(queue->node_region == NULL) {
        die("equeue_create(%ld, %ld, %ld) failed: malloc(%ld) failed: %s", max_size, block_size, threads,
                                                                           (sizeof(struct equeue_node) * max_size),
                                                                           strerror(errno));
    }

    queue->head = NULL;
    queue->tail = NULL;

    for(iter = 0; iter < max_size; iter++) {
        _equeue_create_add_node(queue, iter);
    }

    queue->tail->next = queue->head;
    queue->tail = queue->head;

    queue->max_size = max_size;
    queue->block_size = block_size;

    _equeue_create_block_links(queue);

    queue->allo_size = 0;
    queue->true_size = 0;

    queue->threads = threads;

    queue->cond = econd_create(threads);

    return queue;
}

void equeue_destroy(struct equeue *queue) {
    /* equeue_destroy
     * Destroy the supplied queue.
     *
     * @ struct equeue *queue - The queue to destroy.
     */

    econd_destroy(queue->cond);

    free(queue->node_region);
    free(queue);
}


static struct equeue_node *_equeue_shift(struct equeue_node **cursor) {
    /* _equeue_shift
     * Atomically shift the supplied cursor forward one node.
     *
     * @ struct equeue_node **cursor - The cursor to shift.
     *
     * # struct equeue_node * - The value of the cursor before the shift.
     */

    struct equeue_node *curr, *new;

    curr = eatomic_load(cursor);

    while(1) {
        new = curr->next;

        if(eatomic_compare_exchange(cursor, &curr, new) == true) {
            break;
        }
    }

    return curr;
}

static struct equeue_node *_equeue_shift_block(struct equeue_node **cursor) {
    /* _equeue_shift_block
     * Atomically shift the supplied cursor forward one block.
     *
     * @ struct equeue_node **cursor - The cursor to shift.
     *
     * # struct equeue_node * - The value of the cursor before the shift.
     */

    struct equeue_node *curr, *new;

    curr = eatomic_load(cursor);

    while(1) {
        new = curr->next_block;

        if(eatomic_compare_exchange(cursor, &curr, new) == true) {
            break;
        }
    }

    return curr;
} 

static bool _equeue_size_increment(size_t *size, size_t value, size_t max) {
    /* _equeue_size_increment
     * Atomically increment the supplied size by the supplied value, ensuring that it does not increase beyond max.
     *
     * @ size_t *size - The size to increment.
     * @ size_t value - The value to increment the size by.
     * @ size_t max   - The maximum value for the size.
     *
     * # bool - True if the increment was successful, false otherwise (if it would increase beyond max).
     */

    size_t curr, new;

    curr = eatomic_load(size);

    while(1) {
        if(curr > (max - value)) {
            return false;
        }

        new = curr + value;

        if(eatomic_compare_exchange(size, &curr, new) == true) {
            break;
        }
    }

    return true;
}

static bool _equeue_size_decrement(size_t *size, size_t value, size_t min) {
    /* _equeue_size_decrement
     * Atomically decrement the supplied size by the supplied value, ensuring that it does not decrease below min.
     *
     * @ size_t *size - The size to decrement.
     * @ size_t value - The value to decrement the size by.
     * @ size_t min   - The minimum value for the size
     *
     * # bool - True if the decrement was successful, false otherwise (if it would decrease below min).
     */

    size_t curr, new;

    curr = eatomic_load(size);

    while(1) {
        if(curr < (min + value)) {
            return false;
        }

        new = curr - value;

        if(eatomic_compare_exchange(size, &curr, new) == true) {
            break;
        }
    }

    return true;
}

static void _equeue_await_lock(eatomic_lock_t *lock) {
    /* _equeue_await_lock
     * Wait for the supplied lock to be set.
     *
     * @ eatomic_lock_t *lock - The lock to await.
     */

    while(1) {
        if(eatomic_load(lock) == true) {
            break;
        }
    }
}

int equeue_push(struct equeue *queue, void *element) {
    /* equeue_push
     * Push the supplied element into the supplied queue.
     *
     * @ struct equeue *queue - The queue to push the element into.
     * @ void *element        - The element to push.
     *
     * # int - 0 on success, -1 if there is no room in the queue.
     */

    struct equeue_node *node;

    if(_equeue_size_increment(&queue->allo_size, 1, queue->max_size) == false) {
        /* Increment would overflow, queue is full. */
        return -1;
    }

    node = _equeue_shift(&queue->head);
    eatomic_store(&node->data, element);
    eatomic_lock_acquire(&node->lock);

    if(_equeue_size_increment(&queue->true_size, 1, queue->max_size) == false) {
        die("equeue_push(%p, %p) failed: _equeue_size_increment() failed: Cosmic ray detected", queue, element);
    }

    return 0;
}

int equeue_pop(struct equeue *queue, void **element) {
    /* equeue_pop
     * Pop an element off of the supplied queue.
     *
     * @ struct equeue *queue - The queue to pop an element off of.
     * @ void **element       - The pointer to store the popped element in.
     *
     * # int - 0 on success, -1 if there was no element to pop.
     */

    struct equeue_node *node;

    if(_equeue_size_decrement(&queue->true_size, 1, 0) == false) {
        /* Decrement would underflow, queue is empty. */
        return -1;
    }

    node = _equeue_shift(&queue->tail);
    _equeue_await_lock(&node->lock);
    *element = eatomic_load(&node->data);
    eatomic_lock_release(&node->lock);

    if(_equeue_size_decrement(&queue->allo_size, 1, 0) == false) {
        die("equeue_pop(%p) failed: _equeue_size_decrement() failed: Cosmic ray detected", queue);
    }

    return 0;
}

int equeue_push_block(struct equeue *queue, void **block) {
    /* equeue_push_block
     * Push a block of elements into the supplied queue.
     * The block must be the size of the queue's configured block size.
     *
     * @ struct equeue *queue - The queue to push the block into.
     * @ void **block         - The block to push.
     *
     * # int - 0 on success, -1 if there is no room in the queue.
     */

    struct equeue_node *node;
    size_t iter;

    if(_equeue_size_increment(&queue->allo_size, queue->block_size, queue->max_size) == false) {
        /* Increment would overflow, queue is full. */
        return -1;
    }

    node = _equeue_shift_block(&queue->head);

    for(iter = 0; iter < queue->block_size; iter++) {
        eatomic_store(&node->data, block[iter]);
        eatomic_lock_acquire(&node->lock);
        node = node->next;
    }

    if(_equeue_size_increment(&queue->true_size, queue->block_size, queue->max_size) == false) {
        die("equeue_push_block(%p, %p) failed: _equeue_size_increment() failed: Cosmic ray detected", queue, block);
    }

    return 0;
}

int equeue_pop_block(struct equeue *queue, void **block) {
    /* equeue_pop_block
     * Pop a block off of the supplied queue.
     *
     * @ struct equeue *queue - The queue to pop the block off of.
     * @ void **block         - The pointer to store the popped block in.
     *
     * # int - 0 on success, -1 if there weren't enough elements to pop.
     */

    struct equeue_node *node;
    size_t iter;

    if(_equeue_size_decrement(&queue->true_size, queue->block_size, 0) == false) {
        /* Decrement would underflow, queue is empty (or doesn't have enough for the block). */
        return -1;
    }

    node = _equeue_shift_block(&queue->tail);

    for(iter = 0; iter < queue->block_size; iter++) {
        _equeue_await_lock(&node->lock);
        block[iter] = eatomic_load(&node->data);
        eatomic_lock_release(&node->lock);

        node = node->next;
    }

    if(_equeue_size_decrement(&queue->allo_size, queue->block_size, 0) == false) {
        die("equeue_pop_block(%p, %p) failed: _equeue_size_decrement() failed: Cosmic ray detected", queue, block);
    }

    return 0;
}

size_t equeue_push_many(struct equeue *queue, void **elements, size_t elements_size) {
    /* equeue_push_many
     * Push many elements into the supplied queue.
     *
     * @ struct equeue *queue    - The queue to push the elements into.
     * @ void **elements         - The elements to push.
     * @ size_t elements_size    - The number of elements to push.
     *
     * # size_t - The number of elements successfully pushed.
     */

    size_t elements_pushed;

    elements_pushed = 0;

    /* Push blocks. */
    while((elements_size - elements_pushed) > queue->block_size) {
        if(equeue_push_block(queue, &(elements[elements_pushed])) == -1) {
            break;
        }

        elements_pushed += queue->block_size;
    }

    /* Push remaining elements. */
    while((elements_size - elements_pushed) > 0) {
        if(equeue_push(queue, elements[elements_pushed]) == -1) {
            break;
        }

        elements_pushed += 1;
    }

    return elements_pushed;
}

size_t equeue_pop_many(struct equeue *queue, void **elements, size_t elements_size) {
    /* equeue_pop_many
     * Pop many elements off of the supplied queue.
     *
     * @ struct equeue *queue - The queue to pop the elements off of.
     * @ void **elements      - The array to store the popped elements in.
     * @ size_t elements_size - The maximum number of elements to pop.
     *
     * # size_t - The number of elements successfully popped.
     */

    size_t elements_popped;

    elements_popped = 0;

    /* Pop blocks. */
    while((elements_size - elements_popped) > queue->block_size) {
        if(equeue_pop_block(queue, &elements[elements_popped]) == -1) {
            break;
        }

        elements_popped += queue->block_size;
    }

    /* Pop remaining elements. */
    while((elements_size - elements_popped) > 0) {
        if(equeue_pop(queue, &elements[elements_popped]) == -1) {
            break;
        }

        elements_popped += 1;
    }

    return elements_popped;
}


size_t equeue_get_size(struct equeue *queue) {
    /* equeue_get_size
     * Get the current size of the supplied queue.
     *
     * @ struct equeue *queue - The queue to get the size of.
     *
     * # size_t - The size of the queue.
     */

    return eatomic_load(&queue->true_size);
}


void equeue_wait(struct equeue *queue, struct timespec *timeout) {
    /* equeue_wait
     * Wait for the supplied queue to be signaled.
     *
     * @ struct equeue *queue     - The queue to wait for.
     * @ struct timespec *timeout - The wait timeout.
     */

    econd_wait(queue->cond, timeout);
}

void equeue_signal(struct equeue *queue) {
    /* equeue_signal
     * Signal all threads waiting for the supplied queue.
     *
     * @ struct equeue *queue - The queue to signal.
     */

    econd_broadcast(queue->cond);
}
