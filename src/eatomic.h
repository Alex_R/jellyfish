#ifndef EATOMIC_H_INCLUDED
#define EATOMIC_H_INCLUDED

#include <stdbool.h>


/* Type definitions. */
typedef bool eatomic_lock_t;

/* Macros. */
#define eatomic_load(ptr)       __atomic_load_n(ptr, __ATOMIC_SEQ_CST)
#define eatomic_store(ptr, val) __atomic_store_n(ptr, val, __ATOMIC_SEQ_CST)

#define eatomic_add(ptr, val) __atomic_fetch_add(ptr, val, __ATOMIC_SEQ_CST)
#define eatomic_sub(ptr, val) __atomic_fetch_sub(ptr, val, __ATOMIC_SEQ_CST)

#define eatomic_lock_acquire(ptr) __atomic_test_and_set(ptr, __ATOMIC_SEQ_CST)
#define eatomic_lock_release(ptr) __atomic_clear(ptr, __ATOMIC_SEQ_CST)

#define eatomic_exchange(ptr, val) __atomic_exchange_n(ptr, val, __ATOMIC_SEQ_CST)

#define eatomic_compare_exchange(ptr, expected_val, desired_val) __atomic_compare_exchange_n(ptr, expected_val, desired_val, false, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)

#endif
