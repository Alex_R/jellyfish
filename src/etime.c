#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#include "etime.h"


int _etime_get(struct timespec *time) {
    return clock_gettime(CLOCK_REALTIME, time);
}


int _etime_delta(struct timespec *alpha, struct timespec *beta, struct timespec *delta) {
    /* _etime_delta
     * Calculate the absolute delta between alpha and beta abs(alpha - beta), storing the result in delta.
     * Delta may be NULL.
     *
     * # int - -1 if alpha is bigger than beta, 1 if beta is bigger than alpha, 0 if they are the same.
     */

    int direction;
    struct timespec delta_l;

    if(alpha->tv_sec > beta->tv_sec) {
        direction = -1;
        delta_l.tv_sec = (alpha->tv_sec - beta->tv_sec);
        delta_l.tv_nsec = (alpha->tv_nsec - beta->tv_nsec);
    } else if(alpha->tv_sec < beta->tv_sec) {
        direction = 1;
        delta_l.tv_sec = (beta->tv_sec - alpha->tv_sec);
        delta_l.tv_nsec = (beta->tv_nsec - alpha->tv_nsec);
    } else {
        delta_l.tv_sec = 0;

        if(alpha->tv_nsec > beta->tv_nsec) {
            direction = -1;
            delta_l.tv_nsec = (alpha->tv_nsec - beta->tv_nsec);
        } else if(alpha->tv_nsec < beta->tv_nsec) {
            direction = 1;
            delta_l.tv_nsec = (beta->tv_nsec - alpha->tv_nsec);
        } else {
            direction = 0;
            delta_l.tv_nsec = 0;
        }
    }

    if(delta_l.tv_nsec < 0) {
        delta_l.tv_sec -= 1;
        delta_l.tv_nsec += 1e9;
    }

    if(delta != NULL) {
        memcpy(delta, &delta_l, sizeof(struct timespec));
    }

    return direction;
}
