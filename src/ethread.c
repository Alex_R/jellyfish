#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/prctl.h>
#include <sys/resource.h>

#include "log.h"
#include "die.h"
#include "main.h"
#include "ethread.h"
#include "eatomic.h"
#include "econd.h"


/* Global structure initialisation. */
static struct ethread_context ethread_context = {
    .next_id = 0,
};

__thread static struct ethread_specific_context ethread_specific_context = {
    .id = -1,
};


int ethread_id(void) {
    /* ethread_id
     * Get the thread specific unique ID for the calling thread.
     */

    if(ethread_specific_context.id == -1) {
        ethread_specific_context.id = eatomic_add(&ethread_context.next_id, 1);
    }

    return ethread_specific_context.id;
}


static void *_ethread_starter(void *thread_vd) {
    /* _ethread_starter
     * De-void the supplied ethread pointer and call the enclosed function.
     * Set as the thread start routine in ethread_create().
     */

    struct ethread *thread = (struct ethread *)thread_vd;
    char thread_name[16];

    /* Grab the first 16 bytes of the user defined name. */
    strncpy(thread_name, thread->name, 16);

    /* Since strncpy doesn't add a null terminator if the source string is too long, we make sure there is one. */
    thread_name[15] = '\0';

    /* Set the thread name. */
    if(prctl(PR_SET_NAME, thread_name) == -1) {
        die("_ethread_starter(%p) failed: prctl('%s') failed: %s", thread_vd, thread_name, strerror(errno));
    }

    /* Initialise the thread's ID. */
    ethread_id();

    thread->target_function(thread);

    return thread_vd;
}


struct ethread *ethread_create(ethread_target_function *target_function, const char *thread_name, void *user_data) {
    /* ethread_create
     * Create and start a new thread using the supplied function.
     * The supplied target_function will be supplied a reference to it's containing ethread structure,
     * it must set the 'running' attribute in this structure to 1 after it has started running.
     * The target function must regularly check the 'running' attribute and stop if it changes to 0.
     * The target function must set the "joinable" attribute to 1 when it is about to return.
     *
     * @ ethread_target_function *target_function - A pointer to the function to run in the thread.
     * @ const char *thread_name                  - A name to be set for the new thread.
     * @ void *user_data                          - User data to be stored in the ethread structure passed to the thread.
     *
     * # struct ethread * - The new thread.
     */

    struct ethread *thread;
    size_t iter;
    int ret;

    /* Allocate thread memory */
    thread = malloc(sizeof(struct ethread));
    if(thread == NULL) {
        die("ethread_create(%p, '%s') failed: malloc(%ld) failed: %s", target_function, thread_name, sizeof(struct ethread), strerror(errno));
    }

    /* Init thread structure */
    thread->running = false;
    thread->joinable = false;
    thread->target_function = target_function;
    thread->name = strdup(thread_name);

    thread->user_data = user_data;

    /* Create the thread, via the _ethread_starter function. */
    ret = pthread_create(&thread->thread, NULL, _ethread_starter, (void *)thread);
    if(ret != 0) {
        die("ethread_create(%p, '%s') failed: pthread_create(%p, %p) failed: %s", target_function, thread_name, &thread->thread, _ethread_starter, strerror(ret));
    }

    /* Wait for the thread to start. */
    iter = 0;
    while(!ethread_running(thread)) {
        usleep(10000);
        iter++;

        /* We don't want to deadlock if the thread doesn't start properly, so wait for 5 seconds max. */
        if(iter >= 500) {
            die("ethread_create() failed: Thread failed to start within 5 seconds");
        }
    }

    return thread;
}


void ethread_destroy(struct ethread *thread) {
    /* ethread_destroy
     * Destroy the supplied ethread.
     * The ethread is first gracefully stopped.
     *
     * @ struct ethread *thread - The thread to destroy.
     */

    size_t iter;
    int ret;

    /* Proc the thread to stop. */
    eatomic_store(&thread->running, false);

    /* Wait for the thread to stop. */
    iter = 0;
    while(!eatomic_load(&thread->joinable)) {
        usleep(10000);
        iter++;

        /* We don't want to deadlock if the thread doesn't stop properly, so wait for 60 seconds max. */
        if(iter >= 6000) {
            die("ethread_destroy(%p) failed: Thread failed to stop within 60 seconds");
        }
    }

    /* Join the thread. */
    ret = pthread_join(thread->thread, NULL);
    if(ret != 0) {
        die("ethread_destroy() failed: pthread_join(%p) failed: %s", thread->thread, strerror(ret));
    }

    free(thread->name);
    free(thread);
}

void ethread_destroy_signal(struct ethread *thread, struct econd *cond) {
    /* ethread_destroy_signal
     * Destroy the supplied ethread.
     * The ethread is first gracefully stopped.
     *
     * @ struct ethread *thread - The thread to destroy.
     * @ struct econd *cond     - The cond to signal.
     */

    /* Proc the thread to stop. */
    eatomic_store(&thread->running, false);

    econd_broadcast(cond);

    ethread_destroy(thread);
}


struct ethread_pool *ethread_pool_create(ethread_target_function *target_function, size_t size, const char *pool_name, void *user_data) {
    /* ethread_pool_create
     * Create and start a new ethread pool of the supplied size using the supplied function.
     * See ethread_create for details.
     *
     * @ ethread_target_function *target_function - A pointer to the function to run in the thread.
     * @ size_t size                              - The number of threads to create in the pool.
     * @ const char *pool_name                    - A name to be set for each thread in the new pool.
     * @ void *user_data                          - User data to be stored in the ethread structure passed to the thread.
     *
     * # struct ethread_pool *thread_pool - The new thread pool.
     */

    struct ethread_pool *thread_pool;
    size_t iter;

    /* Allocate pool memory. */
    thread_pool = malloc(sizeof(struct ethread_pool));
    if(thread_pool == NULL) {
        die("ethread_pool_create(%p, %d, '%s') failed: malloc(%ld) failed: %s", target_function, size, pool_name, sizeof(struct ethread_pool), strerror(errno));
    }

    thread_pool->size = size;

    /* Allocate pool threads list memory. */
    thread_pool->threads = malloc(sizeof(struct ethread *) * thread_pool->size);
    if(thread_pool->threads == NULL) {
        die("ethread_pool_create(%p, %d, '%s') failed: malloc(%ld) failed: %s", target_function, size, pool_name, (sizeof(struct ethread *) * thread_pool->size), strerror(errno));
    }

    /* Create child threads. */
    for(iter = 0; iter < thread_pool->size; iter++) {
        thread_pool->threads[iter] = ethread_create(target_function, pool_name, user_data);
    }

    return thread_pool;
}


void ethread_pool_destroy(struct ethread_pool *thread_pool) {
    /* ethread_pool_destroy
     * Destroy the supplied ethread pool.
     * See ethread_destroy for details.
     *
     * @ struct ethread_pool *thread_pool - The pool to destroy.
     */

    size_t iter;

    /* Preemptively proc the threads to stop so we don't have to wait for them all one after another. */
    for(iter = 0; iter < thread_pool->size; iter++) {
        eatomic_store(&thread_pool->threads[iter]->running, false);
    }

    /* Stop the child threads. */
    for(iter = 0; iter < thread_pool->size; iter++) {
        ethread_destroy(thread_pool->threads[iter]);
    }

    free(thread_pool->threads);
    free(thread_pool);
}

void ethread_pool_destroy_signal(struct ethread_pool *thread_pool, struct econd *cond) {
    /* ethread_pool_destroy_signal
     * Destroy the supplied thread pool, while signaling the supplied condition to speed up destruction.
     *
     * @ struct ethread_pool *thread_pool - The thread pool to destroy.
     * @ struct econd *cond               - The condition that the thread pool is waiting on.
     */

    size_t iter;

    for(iter = 0; iter < thread_pool->size; iter++) {
        eatomic_store(&thread_pool->threads[iter]->running, false);
    }

    econd_broadcast(cond);

    ethread_pool_destroy(thread_pool);
}


void _ethread_dynamic_pool_grow(struct ethread_dynamic_pool *thread_pool) {
    /* _ethread_dynamic_pool_grow
     * Grow the supplied dynamic thread pool by 1 thread.
     *
     * @ struct ethread_dynamic_pool *thread_pool - The dynamic thread pool to grow.
     */

    thread_pool->threads[thread_pool->size] = ethread_create(thread_pool->target_function, thread_pool->name, thread_pool->user_data);
    thread_pool->size++;
}

void _ethread_dynamic_pool_shrink(struct ethread_dynamic_pool *thread_pool) {
    /* _ethread_dynamic_pool_shrink
     * Shrink the supplied dynamic thread pool by 1 thread.
     *
     * @ struct ethread_dynamic_pool *thread_pool - The dynamic thread pool to shrink.
     */

    thread_pool->size--;
    ethread_destroy(thread_pool->threads[thread_pool->size]);
}

void _ethread_dynamic_pool_tuner_thread(struct ethread *thread) {
    /* _ethread_dynamic_pool_tuner_thread
     * A tuner for a dynamic pool.
     */

    struct ethread_dynamic_pool *thread_pool = (struct ethread_dynamic_pool *)thread->user_data;
    size_t iter, available;
    struct ethread *pool_thread;
    size_t tune_amount;

    ethread_running_signal(thread);

    while(ethread_running(thread)) {
        usleep(1000);

        available = 0;
        for(iter = 0; iter < thread_pool->size; iter++) {
            pool_thread = thread_pool->threads[iter];

            if(!eatomic_load(&pool_thread->busy)) {
                available++;
            }
        }

        if(available < thread_pool->min_available) {
            tune_amount = (thread_pool->min_available - available);
            if((thread_pool->size + tune_amount) > thread_pool->max_size) {
                tune_amount = (thread_pool->max_size - thread_pool->size);
            }

            if(tune_amount > 0) {
                for(iter = 0; iter < tune_amount; iter++) {
                    _ethread_dynamic_pool_grow(thread_pool);
                }
            }
        } else {
            if(available > thread_pool->max_available) {
                tune_amount = (available - thread_pool->max_available);

                for(iter = 0; iter < tune_amount; iter++) {
                    _ethread_dynamic_pool_shrink(thread_pool);
                }
            }
        }
    }

    ethread_joinable_signal(thread);
}

struct ethread_dynamic_pool *ethread_dynamic_pool_create(ethread_target_function *target_function, size_t max_size, size_t min_available, size_t max_available, const char *pool_name, void *user_data) {
    /* ethread_dynamic_pool_create
     * Allocate and initialise a new dynamic thread pool using the supplied parameters.
     * See ethread_create for details.
     * Threads in a dynamic pool must indicate their busyness using the ethread_busy_* functions.
     *
     * @ ethread_target_function *target_function - A pointer to the function to run in each thread.
     * @ size_t max_size                          - The maximum number of threads to create.
     * @ size_t min_available                     - The minmum number of available (non-busy) threads.
     * @ size_t max_available                     - The maximum number of available (non-busy) threads.
     * @ const char *pool_name                    - A name to be set for each thread.
     * @ void *user_data                          - User data to be passed to the thread.
     *
     * # struct ethread_dynamic_pool *thread_pool - The newly created dynamic thread pool.
     */

    struct ethread_dynamic_pool *thread_pool;

    thread_pool = malloc(sizeof(struct ethread_dynamic_pool));
    if(thread_pool == NULL) {
        die("ethread_dynamic_pool_create(%p, %ld, '%s', %p) failed: malloc(%ld) failed: %s", target_function, max_size, pool_name, user_data, sizeof(struct ethread_dynamic_pool), strerror(errno));
    }

    thread_pool->max_size = max_size;
    thread_pool->size = 0;

    thread_pool->min_available = min_available;
    thread_pool->max_available = max_available;

    thread_pool->target_function = target_function;
    thread_pool->name = strdup(pool_name);
    thread_pool->user_data = user_data;


    thread_pool->threads = malloc(sizeof(struct ethread *) * thread_pool->max_size);
    if(thread_pool->threads == NULL) {
        die("ethread_dynamic_pool_create(%p, %ld, '%s', %p) failed: malloc(%ld) failed: %s", target_function, max_size, pool_name, user_data, (sizeof(struct ethread *) * thread_pool->max_size), strerror(errno));
    }

    thread_pool->tuner_thread = ethread_create(_ethread_dynamic_pool_tuner_thread, "ethread_tuner", (void *)thread_pool);

    return thread_pool;
}

void ethread_dynamic_pool_destroy(struct ethread_dynamic_pool *thread_pool) {
    /* ethread_dynamic_pool_destroy
     * Destroy the supplied dynamic thread pool.
     *
     * @ struct ethread_dynamic_pool *thread_pool - The thread pool to destroy.
     */

    size_t iter;

    ethread_destroy(thread_pool->tuner_thread);

    for(iter = 0; iter < thread_pool->size; iter++) {
        eatomic_store(&thread_pool->threads[iter]->running, false);
    }

    for(iter = 0; iter < thread_pool->size; iter++) {
        ethread_destroy(thread_pool->threads[iter]);
    }

    free(thread_pool->threads);

    free(thread_pool->name);
    free(thread_pool);
}

size_t ethread_dynamic_pool_get_size(struct ethread_dynamic_pool *thread_pool) {
    /* ethread_dynamic_pool_get_size
     * Get the current size of the supplied dynamic thread pool.
     *
     * @ struct ethread_dynamic_pool *thread_pool - The thread pool to get the size of.
     *
     * # size_t - The current size of the dynamic thread pool.
     */

    return eatomic_load(&thread_pool->size);
}


void ethread_running_signal(struct ethread *thread) {
    /* ethread_running_signal
     * Signal the supplied thread's running condition.
     *
     * @ struct ethread *thread - The thread to signal.
     */

    eatomic_store(&thread->running, true);
}

void ethread_joinable_signal(struct ethread *thread) {
    /* ethread_joinable_signal
     * Signal the supplied thread's joinable condition.
     *
     * @ struct ethread *thread - The thread to signal.
     */

    eatomic_store(&thread->joinable, true);
}

bool ethread_running(struct ethread *thread) {
    /* ethread_running
     * Check the supplied thread's running condition.
     *
     * @ struct ethread *thread - The thread to check.
     *
     * # bool - True if the thread is running, False otherwise.
     */

    return eatomic_load(&thread->running);
}


void ethread_busy_set(struct ethread *thread) {
    /* ethread_busy_set
     * Set the busy flag on the supplied thread.
     *
     * @ struct ethread *thread - The thread to set the busy flag on.
     */

    eatomic_store(&thread->busy, true);
}

void ethread_busy_clear(struct ethread *thread) {
    /* ethread_busy_clear
     * Clear the busy flag on the supplied thread.
     *
     * @ struct ethread *thread - The thread to clear the busy flag on.
     */

    eatomic_store(&thread->busy, false);
}
