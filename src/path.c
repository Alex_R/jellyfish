#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>

#include "path.h"


int path_join(const char *path_a, const char *path_b, char *dest, size_t dest_size) {
    /* path_join
     * Join the two supplied paths, storing the resultant path in the caller allocated dest buffer.
     *
     * @ const char *path_a - The first path component to join.
     * @ const char *path_b - The second path component to join.
     * @ char *dest         - The caller allocated buffer to store the resultant path in.
     * @ size_t dest_size   - The size of the caller allocated buffer to store the path in.
     *
     * # int - 0 on success, -1 if the caller allocated buffer is not big enough.
     */

    size_t path_a_size, path_b_size;

    path_a_size = strlen(path_a);
    path_b_size = strlen(path_b);
    if((path_a_size + 1 + path_b_size + 1) >= dest_size) {
        goto error;
    }

    memcpy(dest, path_a, path_a_size);

    if(path_a[path_a_size - 1] != '/' && path_b[0] != '/') { /* No existing slash at the end or start of the paths. */
        dest[path_a_size] = '/';
        path_a_size += 1;
    } else if(path_a[path_a_size - 1] == '/' && path_b[0] == '/') { /* Both have existing slashes. */
        path_a_size -= 1;
    }

    memcpy(&dest[path_a_size], path_b, path_b_size);
    dest[path_a_size + path_b_size] = '\0';

    return 0;

    error:
        return -1;
}


int path_pivot(const char *path, const char *source_prefix, const char *dest_prefix, char *dest, size_t dest_size) {
    /* path_pivot
     * Pivot the supplied path from the supplied source prefix to the supplied dest prefix, storing the result in the caller allocated buffer.
     *
     * @ const char *path          - The path to pivot, must be prefixed by the source prefix.
     * @ const char *source_prefix - The source prefix to pivot from.
     * @ const char *parent_dest   - The dest prefix to pivot to.
     * @ char *dest                - The caller allocated buffer to store the resultant path in.
     * @ size_t dest_size          - The size of the caller allocated buffer to store the path in.
     *
     * # int - 0 on success, -1 if the caller allocated buffer is not big enough.
     */

    return path_join(dest_prefix, &path[strlen(source_prefix)], dest, dest_size);
}


int path_basename(const char *path, char *dest, size_t dest_size) {
    /* path_basename
     * Extract the basename component of the supplied path, storing the result in the supplied caller allocated buffer.
     *
     * @ const char *path - The path to extract the basename component from.
     * @ char *dest       - The caller allocated buffer to store the extracted component in.
     * @ size_t dest_size - The size of the caller allocated buffer to store the path in.
     *
     * # int - 0 on success, -1 if the caller allocated buffer is not big enough.
     */

    const char *basename;
    size_t basename_size;

    basename = strrchr(path, '/');
    if(basename == NULL) {
        basename = path;
    } else {
        basename++;
    }
    basename_size = strlen(basename);

    if((basename_size + 1) >= dest_size) {
        goto error;
    }

    memcpy(dest, basename, basename_size);
    dest[basename_size] = '\0';

    return 0;

    error:
        return -1;
}


bool path_is_prefixed(const char *path, const char *prefix) {
    /* path_is_prefixed
     * Check if the supplied path is prefixed by the supplied prefix.
     *
     * @ const char *path   - The path to check for the prefix.
     * @ const char *prefix - The prefix to check in the path.
     *
     * # bool - True if the path is prefixed, False otherwise.
     */

    size_t path_size, prefix_size;

    path_size = strlen(path);
    prefix_size = strlen(prefix);
    if(prefix_size > path_size) {
        return false;
    }

    if(memcmp(path, prefix, prefix_size) != 0) {
        return false;
    }

    return true;
}


bool path_is_suffixed(const char *path, const char *suffix) {
    /* path_is_suffixed
     * Check if the supplied path is suffixed by the supplied suffix.
     *
     * @ const char *path   - The path to check for the suffix.
     * @ const char *suffix - The suffix to check in the path.
     *
     * # bool - True if the path is suffixed, False otherwise.
     */

    size_t path_size, suffix_size;

    path_size = strlen(path);
    suffix_size = strlen(suffix);
    if(suffix_size > path_size) {
        return false;
    }

    if(memcmp(&path[path_size - suffix_size], suffix, suffix_size) != 0) {
        return false;
    }

    return true;
}
