#ifndef ETHREAD_H_INCLUDED
#define ETHREAD_H_INCLUDED

#include <pthread.h>
#include <stdbool.h>

#include "econd.h"


/* Type definitions. */
struct ethread; /* ethread_target_function and struct ethread have a circular dependancy, so we prototype struct ethread first. */

typedef void (ethread_target_function)(struct ethread *thread);

struct ethread {
    pthread_t thread;
    bool running, joinable, busy;
    ethread_target_function *target_function;
    char *name;
    void *user_data;
};

struct ethread_pool {
    struct ethread **threads;
    size_t size;
};

struct ethread_dynamic_pool {
    struct ethread **threads;
    size_t max_size, min_available, max_available, size;

    ethread_target_function *target_function;
    char *name;
    void *user_data;

    struct ethread *tuner_thread;
};


/* Global structures. */
struct ethread_context {
    int next_id;
};

struct ethread_specific_context {
    int id;
};

#ifdef DEBUG_PUBLIC_CONTEXT
extern struct ethread_context ethread_context;
#endif


/* Function prototypes */
int ethread_id(void);

struct ethread *ethread_create(ethread_target_function *target_function, const char *thread_name, void *user_data);
void ethread_destroy(struct ethread *thread);
void ethread_destroy_signal(struct ethread *thread, struct econd *cond);

struct ethread_pool *ethread_pool_create(ethread_target_function *target_function, size_t size, const char *pool_name, void *user_data);
void ethread_pool_destroy(struct ethread_pool *thread_pool);
void ethread_pool_destroy_signal(struct ethread_pool *thread_pool, struct econd *cond);

struct ethread_dynamic_pool *ethread_dynamic_pool_create(ethread_target_function *target_function, size_t max_size, size_t min_available, size_t max_available, const char *pool_name, void *user_data);
void ethread_dynamic_pool_destroy(struct ethread_dynamic_pool *thread_pool);
size_t ethread_dynamic_pool_get_size(struct ethread_dynamic_pool *thread_pool);

void ethread_running_signal(struct ethread *thread);
void ethread_joinable_signal(struct ethread *thread);
bool ethread_running(struct ethread *thread);

void ethread_busy_set(struct ethread *thread);
void ethread_busy_clear(struct ethread *thread);


#endif
