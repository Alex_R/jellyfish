#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "eref.h"
#include "eatomic.h"
#include "die.h"


void eref_init(struct eref *ref, void *object, eref_destructor_function_t *destructor) {
    /* eref_init
     * Initialise the supplied ref.
     * The ref is initialised with a single reference (for the caller).
     *
     * @ struct eref *ref                       - The ref to initialise.
     * @ struct *object                         - The object to reference.
     * @ eref_destructor_function_t *destructor - The destructor function to be called when the object is unreferenced.
     */

    ref->references = 1;
    ref->object = object;
    ref->destructor = destructor;
}


int eref_acquire(struct eref *ref) {
    /* eref_acquire
     * Acquire the supplied reference.
     *
     * @ struct eref *ref - The reference to acquire.
     *
     * # int - 0 on success, -1 on error (another thread won the race to deference the ref so the object is currently being destroyed).
     */

    size_t references;

    references = eatomic_load(&ref->references);
    while(1) {
        if(references == 0) {
            goto error;
        }

        if(eatomic_compare_exchange(&ref->references, &references, (references + 1))) {
            break;
        }
    }

    return 0;

    error:
        return -1;
}

void eref_release(struct eref *ref) {
    /* eref_release
     * Release the supplied ref (previously acquired with eref_acquire()).
     * If this release causes the ref to be deferenced, the referenced object will be destroyed.
     *
     * @ struct eref *ref - The ref to release.
     */

    size_t references;

    references = eatomic_load(&ref->references);

    while(1) {
        if(references == 0) {
            die("eref_release() failed: Decrement would underflow");
        }

        if(eatomic_compare_exchange(&ref->references, &references, (references - 1))) {
            break;
        }
    }

    if((references - 1) == 0) {
        ref->destructor(ref->object);
    }
}
