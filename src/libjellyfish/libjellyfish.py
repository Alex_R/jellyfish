#!/usr/bin/python

import subprocess
import fcntl
import os
import time


class Jellyfish(object):
    def __init__(self, left_root_path, right_root_path,
                       binary_path="jellyfish", status_format="line",
                       log_level="info",
                       index_max_depth=-1, index_pipeline_size=(1 << 10),
                       index_left_threads=1, index_right_threads=1,
                       index_check_size=True, index_check_mtime=True, index_mtime_window=0,
                       index_inter_resolve="auto",
                       sync_left_threads=1, sync_right_threads=1, sync_pipeline_size=(1 << 10),
                       sync_left_action="copy", sync_right_action="none",
                       sync_left_delay=0, sync_right_delay=0):
        """ .__init__
            Initialise the Jellyfish instance.

            @ str left_root_path  - The left root path to sync.
            @ str right_root_path - The right root path to sync.
            @ str binary_path     - The path to the jellyfish binary.
            @ str status_format   - The status format, "line", or "sum".

            --- See jellyfish help text for remaining options ---
        """

        self.status_format = status_format
        if(self.status_format not in ["line", "sum"]):
            raise ValueError("Supplied 'status_format' is not 'line' or 'sum'")

        self.process = subprocess.Popen([binary_path,
                                            "--log_level", log_level,

                                            "--index_max_depth",     str(index_max_depth),
                                            "--index_pipeline_size", str(index_pipeline_size),
                                            "--index_left_threads",  str(index_left_threads),
                                            "--index_right_threads", str(index_right_threads),
                                            "--index_check_size",    str(index_check_size),
                                            "--index_check_mtime",   str(index_check_mtime),
                                            "--index_mtime_window",  str(index_mtime_window),
                                            "--index_inter_resolve", str(index_inter_resolve),

                                            "--sync_left_threads",  str(sync_left_threads),
                                            "--sync_right_threads", str(sync_right_threads),
                                            "--sync_pipeline_size", str(sync_pipeline_size),
                                            "--sync_left_action",   sync_left_action,
                                            "--sync_right_action",  sync_right_action,
                                            "--sync_left_delay",    str(sync_left_delay),
                                            "--sync_right_delay",   str(sync_right_delay),

                                            "--sync_status_output", "mline" if self.status_format == "line" else "msum",
                                        ] + [left_root_path, right_root_path], stdin=subprocess.PIPE,
                                                                               stdout=subprocess.PIPE,
                                                                               stderr=subprocess.PIPE)

        flags = fcntl.fcntl(self.process.stdout.fileno(), fcntl.F_GETFL)
        fcntl.fcntl(self.process.stdout.fileno(), fcntl.F_SETFL, (flags | os.O_NONBLOCK))

        flags = fcntl.fcntl(self.process.stderr.fileno(), fcntl.F_GETFL)
        fcntl.fcntl(self.process.stderr.fileno(), fcntl.F_SETFL, (flags | os.O_NONBLOCK))

        self.partial_stdout = b""

    def __del__(self):
        self.process.terminate()
        self.process.wait()

    def _get_state(self):
        """ ._get_state
            Get the state from the Jellyfish instance.

            # int - 0 if the instance is running, -1 if the instance has failed, 1 if the instance has finished.
        """

        if(self.process.poll() is None):
            return 0;

        if(self.process.returncode == 0):
            return 1
        else:
            return -1

    def _get_status(self, timeout=None):
        """ ._get_status
            Get status entries from the Jellyfish instance.
            Note that this function returns either JellyfishEntry or JellyfishStatus objects depending on
            the configured status_format.

            @ int timeout - The timeout (in seconds) to wait for status entries, or None to wait forever.

            # [(JellyfishEntry or JellyfishStatus), ...] - The status entries.

            ! IOError
        """

        if(self.process.poll() is not None):
            if(self.process.returncode != 0):
                raise IOError("jellyfish process died: '{}'".format(self.process.stderr.read()))

        elapsed = 0
        while True:
            stdout = self.process.stdout.read()
            if(stdout is not None):
                break
            elapsed += 0.1
            if(timeout is not None):
                if(elapsed >= timeout):
                    return []
            time.sleep(0.1)

        blocks = (self.partial_stdout + stdout).split(b"\0\0")

        entries = []
        for index, entry in enumerate(blocks):
            items = entry.split(b"\0")
            if(len(items) < (3 if self.status_format == "line" else 7)):
                if(index != (len(blocks) - 1)):
                    raise IOError("Block {} (of {}) is incomplete".format(index, len(blocks)))

                self.partial_stdout = entry
                break

            if(self.status_format == "line"):
                entries.append(JellyfishEntry(int(items[0]), int(items[1]), items[2]))
            else:
                entries.append(JellyfishStatus(*map(int, items)))

        return entries

    def status(self):
        first = True
        while(self._get_state() == 0 or first):
            first = False
            entries = self._get_status()

            # We only care about the most recent entry with the sum format
            if(self.status_format == "sum" and len(entries) > 0):
                entries = [entries[-1]]

            for entry in entries:
                yield entry


class JellyfishEntry(object):
    def __init__(self, direction, status, path):
        """ .__init__
            Initialise the JellyfishEntry instance.

            @ int direction - The direction of the entry (o for left, 1 for right, 2 for intersect).
            @ int status    - The status of the entry (0 or negative errno).
            @ bytes path    - The path to the entry (relative to the original root).
        """

        self.direction = direction
        self.status    = status
        self.path      = path

class JellyfishStatus(object):
    def __init__(self, left_success, left_failed, left_bytes, right_success, right_failed, right_bytes, intersect):
        """ .__init__
            Initialise the JellyfishStatus instance.

            --- See Jellyfish help text ---
        """

        self.left_success  = left_success
        self.left_failed   = left_failed
        self.left_bytes    = left_bytes
        self.right_success = right_success
        self.right_failed  = right_failed
        self.right_bytes   = right_bytes
        self.intersect     = intersect
