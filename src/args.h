#ifndef ARGS_H_INCLUDED
#define ARGS_H_INCLUDED

#include <limits.h>
#include <stdbool.h>

#include "main.h"
#include "sync.h"


/* Constants. */
#define ARGS_END   -1
#define ARGS_EXTRA -2


/* Type definitionns. */
struct args {
    int log_level;

    char left_root_path[PATH_MAX], right_root_path[PATH_MAX];

    size_t index_max_depth;
    size_t index_pipeline_size, index_right_threads, index_left_threads;
    bool index_check_size, index_check_mtime;
    struct timespec index_mtime_window;
    enum index_delta_inter_method index_inter_resolve;

    size_t sync_left_threads, sync_right_threads;
    size_t sync_pipeline_size;

    enum sync_action sync_left_action, sync_right_action;

    struct timespec sync_left_delay, sync_right_delay;

    enum main_status_format sync_status_output;
};


/* Function prototypes. */
struct args *args_parse(int argc, char *argv[]);
void args_destroy(struct args *arguments);

#endif
