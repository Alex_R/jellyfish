#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <unistd.h>
#include <fcntl.h>

#include "sync.h"
#include "log.h"
#include "ethread.h"
#include "index.h"
#include "etime.h"


static void _sync_left_worker(struct ethread *thread);
static void _sync_right_worker(struct ethread *thread);

struct sync *sync_create(const char *left_root_path, const char *right_root_path,
                         size_t queue_size,
                         struct econd *next_cond,
                         void *left_context, void *right_context,
                         sync_popfunc_t *left_pop, sync_popfunc_t *right_pop,
                         sync_waitfunc_t *left_wait, sync_waitfunc_t *right_wait,
                         size_t left_worker_threads, size_t right_worker_threads,
                         enum sync_action left_action, enum sync_action right_action,
                         struct timespec left_delay, struct timespec right_delay) {
    /* sync_create
     * Create a new sync context.
     *
     * @ size_t queue_size             - The size of the done/fail queues.
     * @ struct econd *next_cond       - The cond to signal the next item in the pipeline.
     * @ void *left_context            - The left hand side context.
     * @ void *right_context           - The right hand side context.
     * @ sync_popfunc_t *left_pop      - The left hand side pop function (entries to be transfered to dest).
     * @ sync_popfunc_t *right_pop     - The right hand side pop function (entries to be deleted from dest).
     * @ sync_waitfunc_t *left_wait    - The left hand side wait function (entries to be transfered to dest).
     * @ sync_waitfunc_t *right_wait   - The right hand side wait function (entries to be deleted from dest).
     * @ size_t left_worker_threads    - The number of worker threads to spawn for the left hand side of the sync.
     * @ size_t right_worker_threads   - The number of worker threads to spawn for the right hand side of the sync.
     * @ enum sync_action left_action  - The action to perform to resolve left side sync.
     * @ enum sync_action right_action - The action to perform to resolve right side sync.
     * @ struct timespec left_delay    - The file atime delay to wait for (left side).
     * @ struct timespec right_delay   - The file atime delay to wait for (right side)
     *
     * # struct sync * - The newly created sync object, or NULL on error.
     */

    struct sync *sync;

    sync = malloc(sizeof(struct sync));
    if(sync == NULL) {
        erro("sync_create() failed: malloc(%zu) failed: %s", sizeof(struct sync), strerror(errno));
        goto error;
    }

    sync->next_cond = next_cond;

    sync->left_context = left_context;
    sync->right_context = right_context;

    sync->left_pop = left_pop;
    sync->right_pop = right_pop;
    sync->left_wait = left_wait;
    sync->right_wait = right_wait;

    sync->left_action = left_action;
    sync->right_action = right_action;

    sync->left_delay = left_delay;
    sync->right_delay = right_delay;
    memset(&sync->left_root_time, 0, sizeof(struct timespec));
    memset(&sync->right_root_time, 0, sizeof(struct timespec));

    sync->left_dfd = openat(AT_FDCWD, left_root_path, (O_RDONLY | O_DIRECTORY));
    if(sync->left_dfd == -1) {
        erro("sync_create() failed: openat('%s') failed: %s", left_root_path, strerror(errno));
        goto free_sync_and_error;
    }
    sync->right_dfd = openat(AT_FDCWD, right_root_path, (O_RDONLY | O_DIRECTORY));
    if(sync->right_dfd == -1) {
        erro("sync_create() failed: openat('%s') failed: %s", right_root_path, strerror(errno));
        goto free_leftdfd_sync_and_error;
    }

    sync->left_workers_active = left_worker_threads;
    sync->left_workers_failed = 0;
    sync->right_workers_active = right_worker_threads;
    sync->right_workers_failed = 0;

    sync->left_out_queue = equeue_create(queue_size, 1, left_worker_threads);
    sync->right_out_queue = equeue_create(queue_size, 1, right_worker_threads);

    sync->left_thread_pool = ethread_pool_create(_sync_left_worker,   left_worker_threads,
                                                                      "sync_left", (void *)sync);
    sync->right_thread_pool = ethread_pool_create(_sync_right_worker, right_worker_threads ,
                                                                      "sync_right", (void *)sync);

    return sync;

    free_leftdfd_sync_and_error:
        if(close(sync->left_dfd) == -1) {
            erro("sync_create() failed: close() failed: %s", strerror(errno));
        }
    free_sync_and_error:
        free(sync);
    error:
        return NULL;
}

void sync_destroy(struct sync *sync) {
    /* sync_destroy
     * Destroy the supplied sync context.
     *
     * @ struct sync *sync - The sync context to destroy.
     */

    struct index_entry *entry;

    ethread_pool_destroy(sync->left_thread_pool);
    ethread_pool_destroy(sync->right_thread_pool);

    while(equeue_get_size(sync->right_out_queue) > 0) {
        if(equeue_pop(sync->right_out_queue, (void **)&entry) == 0) {
            index_entry_destroy(entry);
        }
    }
    equeue_destroy(sync->right_out_queue);

    while(equeue_get_size(sync->left_out_queue) > 0) {
        if(equeue_pop(sync->left_out_queue, (void **)&entry) == 0) {
            index_entry_destroy(entry);
        }
    }
    equeue_destroy(sync->left_out_queue);

    if(close(sync->right_dfd) == -1) {
        erro("sync_destroy() failed: close() failed: %s", strerror(errno));
    }
    if(close(sync->left_dfd) == -1) {
        erro("sync_destroy() failed: close() failed: %s", strerror(errno));
    }

    free(sync);
}


static int _sync_pop(struct equeue *queue, struct index_entry **entry,
                     size_t *workers_active, size_t *workers_failed) {
    /* _sync_pop
     * Pop an entry from the supplied sync's queue.
     *
     * @ struct equeue *queue       - The queue to pop the entry off of.
     * @ struct index_entry **entry - A pointer to an index_entry to store the entry ptr in.
     * @ size_t *workers_active     - The number of active workers.
     * @ size_t *workers_failed     - The number of failed workers.
     *
     * # int - 0 on success, 1 if the queue is (currently) empty, 2 if the sync is finished,
               -1 if one or more worker threads have failed.
     */

    if(eatomic_load(workers_failed) > 0) {
        return -1;
    }

    if(equeue_pop(queue, (void **)entry) == -1) {
        if(eatomic_load(workers_active) == 0) {
            return 2;
        }

        return 1;
    }

    return 0;
}

int sync_left_pop(struct sync *sync, struct index_entry **entry) {
    return _sync_pop(sync->left_out_queue, entry, &sync->left_workers_active, &sync->left_workers_failed);
}

int sync_right_pop(struct sync *sync, struct index_entry **entry) {
    return _sync_pop(sync->right_out_queue, entry, &sync->right_workers_active, &sync->right_workers_failed);
}


int _sync_copy_file(struct ethread *thread, int dest_dfd, struct index_entry *entry) {
    /* _sync_copy_file
     * Sync the supplied file entry from one side of the transfer to the other.
     *
     * @ struct ethread *thread    - The thread the sync is running in.
     * @ int dest_dfd              - The destination directory FD.
     * @ struct index_entry *entry - The entry to sync.
     *
     * # int - 0 on success, -1 on error.
     */

    int ret;
    int source_fd, dest_fd;

    source_fd = openat(((entry->root != NULL) ? entry->root->dfd : AT_FDCWD), entry->name, (O_RDONLY | O_NOFOLLOW));
    if(source_fd == -1) {
        ret = errno;
        debu("_sync_copy_file() failed: openat('%s') failed: %s", entry->path, strerror(errno));
        goto error;
    }

    dest_fd = openat(dest_dfd, entry->path, (O_WRONLY | O_CREAT | O_NOFOLLOW), entry->st.st_mode);
    if(dest_fd == -1) {
        ret = errno;
        debu("_sync_copy_file() failed: openat('%s') failed: %s", entry->path, strerror(errno));
        goto close_source_and_error;
    }

    ret = -1;
    while(ret != 0) {
        if(!ethread_running(thread)) {
            ret = -EINTR;
            debu("_sync_copy_file() failed: Thread interrupted");
            goto close_dest_source_and_error;
        }

        ret = sendfile(dest_fd, source_fd, NULL, (sysconf(_SC_PAGE_SIZE) * 256));
        if(ret == -1) {
            ret = errno;
            debu("_sync_copy_file() failed: sendfile() failed: %s", strerror(errno));
            goto close_dest_source_and_error;
        }
    }

    if(close(dest_fd) == -1) {
        ret = errno;
        debu("_sync_copy_file() failed: close() failed: %s", strerror(errno));
        goto close_source_and_error;
    }

    if(close(source_fd) == -1) {
        ret = errno;
        debu("_sync_copy_file() failed: close() failed: %s", strerror(errno));
        goto error;
    }

    return 0;

    close_dest_source_and_error:
        if(close(dest_fd) == -1) {
            debu("_sync_copy_file() failed: close() failed: %s", strerror(errno));
        }
    close_source_and_error:
        if(close(source_fd) == -1) {
            debu("_sync_copy_file() failed: close() failed: %s", strerror(errno));
        }
    error:
        return -ret;
}

static int _sync_copy_dir(int dest_dfd, struct index_entry *entry) {
    /* _sync_copy_dir
     * Sync the supplied directory file entry from one side of the transfer to the other.
     *
     * @ int dest_dfd              - The destination directory FD.
     * @ struct index_entry *entry - The entry to sync.
     *
     * # int - 0 on success, a negative error value on error.
     */

    int ret;

    if(mkdirat(dest_dfd, entry->path, entry->st.st_mode) == -1) {
        if(errno != EEXIST) {
            ret = errno;
            debu("_sync_left_dir() failed: mkdirat('%s') failed: %s", entry->path, strerror(errno));
            goto error;
        }
    }

    return 0;

    error:
        return -ret;
}

static int _sync_copy_symlink(int dest_dfd, struct index_entry *entry) {
    /* _sync_copy_symlink
     * Sync the supplied symlink file entry from one side of the transfer to the other.
     *
     * @ int dest_dfd              - The destination directory FD.
     * @ struct index_entry *entry - The entry to sync.
     *
     * # int - 0 on success, a negative error value on error.
     */

    int ret;
    char target[PATH_MAX];

    if(readlinkat(((entry->root != NULL) ? entry->root->dfd : AT_FDCWD), entry->name, target, PATH_MAX) == -1) {
        ret = errno;
        debu("_sync_left_symlink() failed: readlinkat('%s') failed: %s", entry->path, strerror(errno));
        goto error;
    }

    if(symlinkat(target, dest_dfd, entry->path) == -1) {
        ret = errno;
        debu("_sync_left_symlink() failed: symlinkat('%s') failed: %s", entry->path, strerror(errno));
        goto error;
    }

    return 0;

    error:
        return -ret;
}

static int _sync_copy(struct ethread *thread, int dest_dfd, struct index_entry *entry) {
    /* _sync_copy
     * Sync the supplied file entry by copying it from one side of the transfer to the other.
     *
     * @ struct ethread *thread    - The thread the sync is running in.
     * @ int dest_dfd              - The destination root directory FD.
     * @ struct index_entry *entry - The entry to sync.
     *
     * # int - 0 on success, a negative error value on error.
     */

    int ret;

    /* File may already exist and be of a different type to the source, so ensure it's removed. */
    if(unlinkat(dest_dfd, entry->path, 0) == -1) {
        if(errno != ENOENT && errno != EISDIR) {
            ret = errno;
            debu("_sync_copy() failed: unlinkat('%s') failed: %s", entry->path, strerror(errno));
            goto error;
        }
    }

    if(unlinkat(dest_dfd, entry->path, AT_REMOVEDIR) == -1) {
        if(errno != ENOENT && errno != ENOTEMPTY) {
            ret = errno;
            debu("_sync_copy() failed: unlinkat('%s') failed: %s", entry->path, strerror(errno));
            goto error;
        }
    }


    switch(entry->st.st_mode & S_IFMT) {
        case S_IFREG:
            debu("[sync_copy] start file '%s'", entry->path);
            ret = _sync_copy_file(thread, dest_dfd, entry);
            if(ret < 0) {
                ret = -ret;
                goto error;
            }
            break;

        case S_IFDIR:
            debu("[sync_copy] start dir '%s'", entry->path);
            ret = _sync_copy_dir(dest_dfd, entry);
            if(ret < 0) {
                ret = -ret;
                goto error;
            }
            break;

        case S_IFLNK:
            debu("[sync_copy] start symlink '%s'", entry->path);
            ret = _sync_copy_symlink(dest_dfd, entry);
            if(ret < 0) {
                ret = -ret;
                goto error;
            }
            break;

        case S_IFSOCK:
        case S_IFBLK:
        case S_IFCHR:
        case S_IFIFO:
            ret = EIO;
            debu("[sync_copy] entry '%s' unsyncable (bad type)", entry->path);
            goto error;

        default:
            ret = EIO;
            debu("_sync_copy() failed: Cosmic ray detected (entry mode file type is invalid)");
            goto error;
    }

    if(fchownat(dest_dfd, entry->path, entry->st.st_uid, entry->st.st_gid, AT_SYMLINK_NOFOLLOW) == -1) {
        ret = errno;
        debu("_sync_copy() failed: fchownat('%s') failed: %s", entry->path, strerror(errno));
        goto error;
    }

    if(utimensat(dest_dfd, entry->path, (struct timespec [2]){entry->st.st_atim, entry->st.st_mtim},
                                        AT_SYMLINK_NOFOLLOW) == -1) {
        ret = errno;
        debu("_sync_copy() failed: utimensat('%s') failed: %s", entry->path, strerror(errno));
        goto error;
    }

    /* Our copying of the entry will have updated the parent root's times, so we need to fix them up. */
    if(entry->last_in_root) {
        if(utimensat(dest_dfd, entry->root->full_path,
                               (struct timespec [2]){entry->root->st.st_atim, entry->root->st.st_mtim},
                               AT_SYMLINK_NOFOLLOW) == -1) {
            ret = errno;
            debu("_sync_copy() failed: utimensat('%s') failed: %s", entry->path, strerror(errno));
            goto error;
        }
    }

    return 0;

    error:
        return -ret;
}

static int _sync_delete(struct index_entry *entry) {
    /* _sync_delete
     * Sync the supplied entry by deleting it.
     *
     * @ struct index_entry *entry - The entry to sync.
     *
     * # int - 0 on success, a negative error value on error.
     */

    int ret, source_dfd;

    debu("[sync_delete] '%s'", entry->path);

    source_dfd = ((entry->root != NULL) ? entry->root->dfd : AT_FDCWD);

    if(unlinkat(source_dfd, entry->name, 0) == -1) {
        if(errno != ENOENT && errno != EISDIR) {
            ret = errno;
            debu("_sync_delete() failed: unlinkat('%s') failed: %s", entry->path, strerror(errno));
            goto error;
        }

        if(errno == EISDIR) {
            if(unlinkat(source_dfd, entry->name, AT_REMOVEDIR) == -1) {
                if(errno != ENOENT && errno != ENOTEMPTY) {
                    ret = errno;
                    debu("_sync_delete() failed: unlinkat('%s') failed: %s", entry->path, strerror(errno));
                    goto error;
                }
            }
        }
    }

    /* Attempt to remove parent directory, except for the original roots. */
    if(entry->last_in_root && entry->root->parent != NULL) {
        if(unlinkat(entry->root->parent->dfd, entry->root->path, AT_REMOVEDIR) == -1) {
            if(errno != ENOTEMPTY) {
                ret = errno;
                debu("_sync_delete() failed: unlinkat('%s') failed: %s", entry->root->path, strerror(errno));
                goto error;
            }
        }
    }

    return 0;

    error:
        return -ret;
}

static int _sync_entry_delay_pass_init(struct timespec *root_time, struct index_entry *entry) {
    char fd_path[PATH_MAX], root_path[PATH_MAX], tmp_path[PATH_MAX];
    int fd;
    struct stat st;

    if(snprintf(fd_path, PATH_MAX, "/proc/self/fd/%d", ((entry->root != NULL) ? entry->root->dfd : AT_FDCWD)) < 1) {
        erro("_sync_entry_delay_pass_init() failed: snprintf() failed: %s", strerror(errno));
        goto error;
    }

    memset(root_path, '\0', (sizeof(char) * PATH_MAX));
    if(readlink(fd_path, root_path, PATH_MAX) == -1) {
        erro("_sync_entry_delay_pass_init() failed: readlink('%s') failed: %s", fd_path, strerror(errno));
        goto error;
    }

    if(snprintf(tmp_path, PATH_MAX, "%s/XXXXXX", root_path) < 1) {
        erro("_sync_entry_delay_pass_init() failed: snprintf() failed: %s", strerror(errno));
        goto error;
    }

    fd = mkstemp(tmp_path);
    if(fd == -1) {
        erro("_sync_entry_delay_pass_init() failed: mkstemp() failed: %s", strerror(errno));
        goto error;
    }

    if(fstat(fd, &st) == -1) {
        erro("_sync_entry_delay_pass_init() failed: fstat() failed: %s", strerror(errno));
        goto close_unlink_and_error;
    }

    memcpy(root_time, &st.st_ctime, sizeof(struct timespec));
    debu("[sync] init root time at %zu.%zu", root_time->tv_sec, root_time->tv_nsec);

    if(close(fd) == -1) {
        erro("_sync_entry_delay_pass_init() failed: close() failed: %s", strerror(errno));
        goto unlink_and_error;
    }

    if(unlink(tmp_path) == -1) {
        erro("_sync_entry_delay_pass_init() failed: unlink() failed: %s", strerror(errno));
        goto error;
    }

    return 0;

    close_unlink_and_error:
        if(close(fd) == -1) {
            erro("_sync_entry_delay_pass_init() failed: close() failed: %s", strerror(errno));
        }
    unlink_and_error:
        if(unlink(tmp_path) == -1) {
            erro("_sync_entry_delay_pass_init() failed: unlink() failed: %s", strerror(errno));
        }
    error:
        return -1;
}

int _sync_entry_delay_pass(struct timespec *root_time, struct index_entry *entry, struct timespec *delay) {
    /* _sync_entry_delay_pass
     * Check if the supplied file entry time is within the supplied delay (relative to the supplied root).
     *
     * @ struct timespec *root_time - The root time to compare the entry time against (may be uninitialised).
     * @ struct index_entry *entry  - The entry to check the time for.
     * @ struct timespec *delay     - The delay that the entry time delta must be within.
     *
     * # int - 0 if the delta is outside the supplied delay, 1 if it's within the delay, -1 on error.
     */

    struct timespec delta;

    if(root_time->tv_sec == 0 && root_time->tv_nsec == 0) {
        if(_sync_entry_delay_pass_init(root_time, entry) == -1) {
            erro("_sync_entry_delay_pass() failed: _sync_entry_delay_pass_init() failed");
            goto error;
        }
    }

    if(_etime_delta(root_time, &entry->st.st_atim, &delta) == 1) {
        return 1;
    }

    if(_etime_delta(&delta, delay, NULL) == 1) {
        return 1;
    }

    return 0;

    error:
        return -1;
}

static int _sync(struct ethread *thread, int dest_dfd, struct index_entry *entry,
                                         enum sync_action action,
                                         struct timespec *delay, struct timespec *root_time) {
    /* _sync
     * Sync the supplied file entry using the supplied action.
     *
     * @ struct ethread *thread     - The thread in which the sync is occuring.
     * @ in dest_dfd                - The destination root dfd, or -1 if there is no destination dfd.
     * @ struct index_entry *entry  - The entry to sync.
     * @ enum sync_action action    - The sync action to perform.
     * @ struct timespec *delay     - The file time delay to wait for.
     * @ struct timespec *root_time - The root time to compare against.
     *
     * # int - 0 on success, a negative error value on error.
     */

    int ret;

    ret = _sync_entry_delay_pass(root_time, entry, delay);
    if(ret == -1) {
        return -1;
    }
    if(ret == 1) {
        debu("[sync] delay skip '%s'", entry->path);
        return 0;
    }

    switch(action) {
        case SYNC_ACTION_NONE:
            return 0;

        case SYNC_ACTION_COPY:
            return _sync_copy(thread, dest_dfd, entry);

        case SYNC_ACTION_DELETE:
            return _sync_delete(entry);
    };
}

static int _sync_worker_blocking_push(struct ethread *thread, struct equeue *queue, struct index_entry *entry) {
    while(1) {
        if(!ethread_running(thread)) {
            erro("_index_entry_blocking_push() failed: Thread interrupted");
            goto error;
        }

        if(equeue_push(queue, entry) == -1) {
            usleep(10);
            continue;
        }

        equeue_signal(queue);
        break;
    }

    return 0;

    error:
        return -1;
}

void _sync_worker(struct ethread *thread, size_t *workers_active, size_t *workers_failed,
                                          void *context, sync_popfunc_t *pop_func, sync_waitfunc_t *wait_func,
                                          int dest_dfd, enum sync_action action,
                                          struct timespec *delay, struct timespec *root_time,
                                          struct equeue *out_queue, struct econd *next_cond) {
    bool active;
    struct index_entry *entry;
    int ret;

    ethread_running_signal(thread);

    active = true;
    while(ethread_running(thread)) {
        if(eatomic_load(workers_failed) != 0) {
            erro("_sync_worker() failed: One or more worker threads failed");
            break;
        }

        ret = pop_func(context, (void **)&entry);
        if(ret == -1) {
            erro("_sync_worker() failed: pop_func() failed");
            goto error;
        }
        if(ret == 1 || ret == 2) {
            if(ret == 2 && active) {
                active = false;
                eatomic_sub(workers_active, 1);
                debu("[sync] worker finished");
            }

            wait_func(context, &(struct timespec){.tv_sec = 0, .tv_nsec = 1e6});
            continue;
        }
        if(!active) {
            active = true;
            eatomic_add(workers_active, 1);
        }

        entry->sync_status = _sync(thread, dest_dfd, entry, action, delay, root_time);

        debu("[sync] done (%d) '%s'", entry->sync_status, entry->path);
        if(_sync_worker_blocking_push(thread, out_queue, entry)) {
            erro("_sync_worker() failed: _sync_worker_blocking_push() failed");
            goto free_entry_and_error;
        }
        econd_broadcast(next_cond);
    }

    end:
        ethread_joinable_signal(thread);
        return;

    free_entry_and_error:
        index_entry_destroy(entry);
    error:
        eatomic_add(workers_failed, 1);
        goto end;
}

void _sync_left_worker(struct ethread *thread) {
    struct sync *sync = (struct sync *)thread->user_data;

    _sync_worker(thread, &sync->left_workers_active, &sync->left_workers_failed,
                         sync->left_context, sync->left_pop, sync->left_wait,
                         sync->right_dfd, sync->left_action, &sync->left_delay, &sync->left_root_time,
                         sync->left_out_queue, sync->next_cond);
}

void _sync_right_worker(struct ethread *thread) {
    struct sync *sync = (struct sync *)thread->user_data;

    _sync_worker(thread, &sync->right_workers_active, &sync->right_workers_failed,
                         sync->right_context, sync->right_pop, sync->right_wait,
                         sync->left_dfd, sync->right_action, &sync->right_delay, &sync->right_root_time,
                         sync->right_out_queue, sync->next_cond);
}
