#ifndef INDEX_H_INCLUDED
#define INDEX_H_INCLUDED

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

#include "equeue.h"
#include "ethread.h"
#include "eref.h"
#include "elock.h"
#include "econd.h"


/* Type definitions. */
struct index {
    struct equeue *root_queue, *entry_queue;

    size_t root_sequential_alloc, root_sequential_done;
    struct econd *root_sequential_cond;
    struct elock *root_queue_lock;

    struct ethread_pool *thread_pool;
    size_t workers_active, workers_failed;

    struct econd *next_cond;
};

struct index_root {
    struct index_root *parent; /* The parent of this root, or NULL if it's a root-root. */
    int dfd;

    size_t sequential_id;

    char path[PATH_MAX];      /* Note that this path is relative to the parent if it's non-NULL. */
    char full_path[PATH_MAX]; /* The full path relative to the original root. */

    struct stat st;

    size_t max_depth;
    struct eref reference;
};

struct index_entry {
    struct index_root *root;

    char name[NAME_MAX];
    char path[PATH_MAX]; /* Note that this path is relative to the original root. */

    bool last_in_root;
    struct stat st;

    int sync_status;
};

enum index_delta_inter_method {
    INDEX_DELTA_INTER_NONE,
    INDEX_DELTA_INTER_LEFT,
    INDEX_DELTA_INTER_RIGHT,
    INDEX_DELTA_INTER_AUTO,
};

enum index_delta_inter_auto_pref {
    INDEX_DELTA_INTER_AUTO_NONE,
    INDEX_DELTA_INTER_AUTO_LEFT,
    INDEX_DELTA_INTER_AUTO_RIGHT,
    INDEX_DELTA_INTER_AUTO_TIME,
};

struct index_delta {
    struct index *index_alpha, *index_beta;

    struct equeue *intersect_queue, *comp_alpha_queue, *comp_beta_queue;

    bool inter_check_size;

    bool inter_check_mtime;
    struct timespec inter_mtime_window;
    enum index_delta_inter_method inter_method;
    enum index_delta_inter_auto_pref inter_auto_pref;

    struct ethread *thread;
    bool worker_active, worker_failed;

    struct econd *last_cond, *next_cond;
};


/* Function prototypes. */
struct index *index_create(size_t queue_size, size_t worker_threads, struct econd *next_cond);
void index_destroy(struct index *index);

int index_root_add(struct index *index, const char *root_path, size_t max_depth);
int index_entry_pop(struct index *index, struct index_entry **entry);
void index_entry_wait(struct index *index, struct timespec *timeout);
void index_entry_destroy(struct index_entry *entry);

struct index_delta *index_delta_create(struct index *alpha, struct index *beta, size_t queue_size,
                                       struct econd *last_cond, struct econd *next_cond,
                                       bool inter_check_size,
                                       bool inter_check_mtime, struct timespec inter_mtime_window,
                                       enum index_delta_inter_method inter_method,
                                       enum index_delta_inter_auto_pref inter_auto_pref);
void index_delta_destroy(struct index_delta *delta);

int index_delta_intersect_pop(struct index_delta *delta, struct index_entry **entry);
int index_delta_comp_alpha_pop(struct index_delta *delta, struct index_entry **entry);
int index_delta_comp_beta_pop(struct index_delta *delta, struct index_entry **entry);

void index_delta_intersect_wait(struct index_delta *delta, struct timespec *timeout);
void index_delta_comp_alpha_wait(struct index_delta *delta, struct timespec *timeout);
void index_delta_comp_beta_wait(struct index_delta *delta, struct timespec *timeout);

#endif
