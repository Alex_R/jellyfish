#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <syslog.h>
#include <stdarg.h>

#include "log.h"
#include "die.h"
#include "main.h"

static struct log_context _log_context = {
    .log_level = LOG_INFO,
};


void _log_internal(int priority, const char *format, ...) {
    /* _log_internal
     * Log a message with the supplied priority, format, and any variadic arguments.
     *
     * @ int priority       - The syslog priority level to log the message with (see LOG_* constants in syslog(3)).
     * @ const char *format - The format of the message.
     * @ ...                - Any extra variadic arguments to use with the format to create the message.
     */

    va_list args;

    if(priority > _log_context.log_level) {
        return;
    }

    va_start(args, format);
    if(priority == LOG_WARNING || priority == LOG_ERR) {
        vfprintf(stderr, format, args);
    } else {
        vfprintf(stdout, format, args);
    }
    va_end(args);
}

void log_level_set(int priority) {
    _log_context.log_level = priority;
}
