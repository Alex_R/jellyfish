#ifndef ETIME_H_INCLUDED
#define ETIME_H_INCLUDED

int _etime_get(struct timespec *time);
int _etime_delta(struct timespec *alpha, struct timespec *beta, struct timespec *delta);

#endif
